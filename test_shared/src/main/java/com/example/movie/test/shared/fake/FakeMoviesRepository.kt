package com.example.movie.test.shared.fake

import com.example.movie.domain.model.Movie
import com.example.movie.domain.model.Resource
import com.example.movie.domain.repository.MoviesRepository
import com.example.movie.test.shared.util.MovieUtil.createMovie
import com.example.movie.test.shared.util.MovieUtil.fakeNowPlayingMovies
import com.example.movie.test.shared.util.MovieUtil.fakePopularMovies
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow

class FakeMoviesRepository(private val isSuccess: Boolean = true) : MoviesRepository {

    override fun getPopularMovies(forceRefresh: Boolean): Flow<Resource<List<Movie>>> {
        return flow {
            emit(Resource.loading())
            emit(resource(fakePopularMovies))
        }
    }

    override fun getPopularMoviesNextPage(): Flow<Resource<Unit>> {
        return flow {
            emit(Resource.loading())
            emit(resource())
        }
    }

    override fun getNowPlayingMovies(forceRefresh: Boolean): Flow<Resource<List<Movie>>> {
        return flow {
            emit(Resource.loading())
            emit(resource(fakeNowPlayingMovies))
        }
    }

    override fun getNowPlayingMoviesNextPage(): Flow<Resource<Unit>> {
        return flow {
            emit(Resource.loading())
            emit(resource())
        }
    }

    override fun getMovie(movieId: Int): Flow<Movie> {
        return flow {
            emit(createMovie(movieId))
        }
    }

    private fun <T> resource(data: T? = null): Resource<T> {
        return if (isSuccess) {
            Resource.success(data)
        } else {
            Resource.error(errorMessage, data)
        }
    }

    companion object {
        const val errorMessage = "error"
    }
}
