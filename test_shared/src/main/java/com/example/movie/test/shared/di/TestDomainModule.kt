package com.example.movie.test.shared.di

import com.example.movie.domain.repository.GenresRepository
import com.example.movie.domain.repository.MoviesRepository
import com.example.movie.domain.usecase.GetMovieUseCase
import com.example.movie.domain.usecase.GetNowPlayingMoviesNextPageUseCase
import com.example.movie.domain.usecase.GetNowPlayingMoviesUseCase
import com.example.movie.domain.usecase.GetPopularMoviesNextPageUseCase
import com.example.movie.domain.usecase.GetPopularMoviesUseCase
import com.example.movie.domain.util.GenresNameFiller
import com.example.movie.test.shared.fake.FakeGenresRepository
import com.example.movie.test.shared.fake.FakeMoviesRepository
import org.koin.core.parameter.parametersOf
import org.koin.dsl.module

val testDomainModule = module {
    factory { GetMovieUseCase(get { parametersOf(true) }, get()) }

    factory { (isSuccess: Boolean) -> GetPopularMoviesUseCase(get { parametersOf(isSuccess) }) }

    factory { (isSuccess: Boolean) ->
        GetPopularMoviesNextPageUseCase(get { parametersOf(isSuccess) })
    }

    factory { (isSuccess: Boolean) ->
        GetNowPlayingMoviesUseCase(
            get { parametersOf(isSuccess) },
            get()
        )
    }

    factory { (isSuccess: Boolean) ->
        GetNowPlayingMoviesNextPageUseCase(get { parametersOf(isSuccess) })
    }

    single<MoviesRepository> { (isSuccess: Boolean) ->
        FakeMoviesRepository(
            isSuccess
        )
    }

    single { GenresNameFiller(get()) }

    single<GenresRepository> { FakeGenresRepository() }
}
