package com.example.movie.test.shared.util

import com.example.movie.domain.model.Genre
import com.example.movie.domain.model.Movie

object GenreUtil {

    const val name = "action"

    val fakeGenreIds = listOf(1)

    val fakeGenresMapper = fakeGenreIds.associateWith { name }

    fun createGenres(ids: List<Int> = fakeGenreIds, name: String = ""): List<Genre> {
        return MutableList(ids.size) { Genre(ids[it], name) }
    }

    fun List<Movie>.fillGenresName() = map { it.fillGenresName() }

    fun Movie.fillGenresName(): Movie {
        val genres = this.genres.map { it.copy(name = fakeGenresMapper[it.id] ?: "") }
        return copy(genres = genres)
    }
}
