package com.example.movie.test.shared.util

import com.example.movie.domain.model.Genre
import com.example.movie.domain.model.Movie
import com.example.movie.test.shared.util.GenreUtil.createGenres

object MovieUtil {

    val fakePopularMovies = createMovieList(listOf(1, 2, 3, 4))

    val fakeNowPlayingMovies = createMovieList(listOf(11, 12, 13, 14))

    val fakeMovie = createMovie(1)

    fun createMovie(id: Int, genres: List<Genre> = createGenres()) = Movie(
        id = id,
        title = "title$id",
        posterUrl = "",
        overview = "",
        genres = genres,
        voteCount = 0,
        voteAverage = 0.0,
        releaseDate = "",
        backdropUrl = ""
    )

    fun createMovieList(ids: List<Int>) = MutableList(ids.size) { createMovie(ids[it]) }.toList()
}
