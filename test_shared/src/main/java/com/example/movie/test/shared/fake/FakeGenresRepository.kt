package com.example.movie.test.shared.fake

import com.example.movie.domain.model.Resource
import com.example.movie.domain.repository.GenresRepository
import com.example.movie.test.shared.util.GenreUtil.fakeGenresMapper
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow

class FakeGenresRepository : GenresRepository {

    override fun getGenresMapper(): Flow<Resource<Map<Int, String>>> {
        return flow {
            emit(Resource.success(fakeGenresMapper))
        }
    }
}
