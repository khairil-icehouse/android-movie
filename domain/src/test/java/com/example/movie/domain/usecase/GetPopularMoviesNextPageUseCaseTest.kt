package com.example.movie.domain.usecase

import com.example.movie.domain.model.Resource
import com.example.movie.domain.repository.MoviesRepository
import com.example.movie.test.shared.util.assertCompleteStream
import io.mockk.every
import io.mockk.mockk
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Before
import org.junit.Test

@ExperimentalCoroutinesApi
class GetPopularMoviesNextPageUseCaseTest {

    private lateinit var useCase: GetPopularMoviesNextPageUseCase

    private val repository = mockk<MoviesRepository>()

    @Before
    fun setupTest() {
        useCase = GetPopularMoviesNextPageUseCase(repository)
    }

    @Test
    fun `get popular movies next page`() = runBlockingTest {
        // Given repository response when getting popular movies next page
        every { repository.getPopularMoviesNextPage() } returns flow<Resource<Unit>> {
            emit(Resource.loading())
            emit(Resource.success())
        }

        // When getting popular movies next page from use case
        val moviesFlow = useCase()

        // Then return resource from repository
        moviesFlow.assertCompleteStream(
            Resource.loading(),
            Resource.success()
        )
    }
}
