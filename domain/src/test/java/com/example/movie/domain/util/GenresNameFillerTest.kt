package com.example.movie.domain.util

import com.example.movie.domain.model.Resource
import com.example.movie.domain.repository.GenresRepository
import com.example.movie.test.shared.util.GenreUtil
import com.example.movie.test.shared.util.GenreUtil.fillGenresName
import com.example.movie.test.shared.util.MovieUtil.fakeMovie
import com.example.movie.test.shared.util.MovieUtil.fakePopularMovies
import com.example.movie.test.shared.util.assertCompleteStream
import io.mockk.every
import io.mockk.mockk
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Before
import org.junit.Test

@ExperimentalCoroutinesApi
class GenresNameFillerTest {

    private lateinit var genresNameFiller: GenresNameFiller

    private val genresRepository = mockk<GenresRepository>()

    @Before
    fun setupTest() {
        genresNameFiller = GenresNameFiller(genresRepository)
        mockGenresRepository()
    }

    @Test
    fun `fill movies with genre names`() = runBlockingTest {
        // Given movies flow without genre names
        val moviesWithoutGenreNames = fakePopularMovies
        val moviesFlow = flow { emit(Resource.success(moviesWithoutGenreNames)) }

        // When filling movies with genre names
        val result = genresNameFiller.fillMovies(moviesFlow)

        // Then movies are filled with genre names
        val expectedMovies = moviesWithoutGenreNames.fillGenresName()
        result.assertCompleteStream(Resource.success(expectedMovies))
    }

    @Test
    fun `fill movie with genre name`() = runBlockingTest {
        // Given movie flow without genre name
        val movie = fakeMovie
        val movieFlow = flow { emit(movie) }

        // When filling movie with genre name
        val result = genresNameFiller.fillMovie(movieFlow)

        // Then movie is filled with genre name
        val expectedMovie = movie.fillGenresName()
        result.assertCompleteStream(expectedMovie)
    }

    private fun mockGenresRepository() {
        every { genresRepository.getGenresMapper() } returns flow {
            emit(Resource.success(GenreUtil.fakeGenresMapper))
        }
    }
}
