package com.example.movie.domain.usecase

import com.example.movie.domain.model.Movie
import com.example.movie.domain.model.Resource
import com.example.movie.domain.repository.MoviesRepository
import com.example.movie.domain.util.GenresNameFiller
import com.example.movie.test.shared.util.GenreUtil.fillGenresName
import com.example.movie.test.shared.util.MovieUtil.fakeNowPlayingMovies
import com.example.movie.test.shared.util.assertCompleteStream
import io.mockk.every
import io.mockk.mockk
import io.mockk.slot
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Before
import org.junit.Test

@ExperimentalCoroutinesApi
class GetNowPlayingMoviesUseCaseTest {

    private lateinit var useCase: GetNowPlayingMoviesUseCase

    private val repository = mockk<MoviesRepository>()

    private val genreNameFiller = mockk<GenresNameFiller>()

    @Before
    fun setupTest() {
        useCase = GetNowPlayingMoviesUseCase(repository, genreNameFiller)
        mockGenreNameFiller()
    }

    @Test
    fun `get now playing movies without force refresh`() = runBlockingTest {
        // Given now playing movies that will be returned by repository
        val movies = fakeNowPlayingMovies
        every { repository.getNowPlayingMovies(false) } returns flow {
            emit(Resource.success(movies))
        }

        // When getting now playing movies from use case
        val moviesFlow = useCase(false)

        // Then return now playing movies from repository
        moviesFlow.assertCompleteStream(Resource.success(movies.fillGenresName()))
    }

    @Test
    fun `get now playing movies with force refresh`() = runBlockingTest {
        // Given now playing movies that will be returned by repository
        val movies = fakeNowPlayingMovies
        every { repository.getNowPlayingMovies(true) } returns flow {
            emit(Resource.success(movies))
        }

        // When getting now playing movies from use case
        val moviesFlow = useCase(true)

        // Then return now playing movies from repository
        moviesFlow.assertCompleteStream(Resource.success(movies.fillGenresName()))
    }

    private fun mockGenreNameFiller() {
        val slot = slot<Flow<Resource<List<Movie>>>>()
        every { genreNameFiller.fillMovies(capture(slot)) } answers {
            slot.captured.map {
                val movies = it.data?.fillGenresName()
                it.copy(data = movies)
            }
        }
    }
}
