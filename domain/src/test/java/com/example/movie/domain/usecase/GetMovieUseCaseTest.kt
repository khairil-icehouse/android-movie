package com.example.movie.domain.usecase

import com.example.movie.domain.model.Movie
import com.example.movie.domain.repository.MoviesRepository
import com.example.movie.domain.util.GenresNameFiller
import com.example.movie.test.shared.util.GenreUtil.fillGenresName
import com.example.movie.test.shared.util.MovieUtil.createMovie
import com.example.movie.test.shared.util.assertCompleteStream
import io.mockk.every
import io.mockk.mockk
import io.mockk.slot
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Before
import org.junit.Test

@ExperimentalCoroutinesApi
class GetMovieUseCaseTest {

    private lateinit var useCase: GetMovieUseCase

    private val repository = mockk<MoviesRepository>()

    private val genreNameFiller = mockk<GenresNameFiller>()

    @Before
    fun setupTest() {
        useCase = GetMovieUseCase(repository, genreNameFiller)

        mockGenreNameFiller()
    }

    @Test
    fun `get movie detail of an id`() = runBlockingTest {
        // Given movie
        val id = 1
        val movie = createMovie(id)
        // Given repository response when getting detail of movie id 1
        every { repository.getMovie(id) } returns flow { emit(movie) }

        // When getting movie detail from usecase
        val movieFlow = useCase(id)

        // Then return movie detail from repository
        movieFlow.assertCompleteStream(movie.fillGenresName())
    }

    private fun mockGenreNameFiller() {
        val slot = slot<Flow<Movie>>()
        every { genreNameFiller.fillMovie(capture(slot)) } answers {
            slot.captured.map { it.fillGenresName() }
        }
    }
}
