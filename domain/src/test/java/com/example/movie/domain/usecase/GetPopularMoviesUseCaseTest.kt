package com.example.movie.domain.usecase

import com.example.movie.domain.model.Resource
import com.example.movie.domain.repository.MoviesRepository
import com.example.movie.test.shared.util.MovieUtil.createMovieList
import com.example.movie.test.shared.util.assertCompleteStream
import io.mockk.every
import io.mockk.mockk
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Before
import org.junit.Test

@ExperimentalCoroutinesApi
class GetPopularMoviesUseCaseTest {

    private lateinit var useCase: GetPopularMoviesUseCase

    private val repository = mockk<MoviesRepository>()

    @Before
    fun setupTest() {
        useCase = GetPopularMoviesUseCase(repository)
    }

    @Test
    fun `get popular movies without force refresh`() = runBlockingTest {
        // Given popular movies that will be returned by repository
        val ids = listOf(1, 2)
        val expectedMovies = createMovieList(ids)
        every { repository.getPopularMovies(false) } returns flow {
            emit(Resource.success(expectedMovies))
        }

        // When getting popular movies from use case
        val moviesFlow = useCase(false)

        // Then return popular movies from repository
        moviesFlow.assertCompleteStream(Resource.success(expectedMovies))
    }

    @Test
    fun `get popular movies with force refresh`() = runBlockingTest {
        // Given popular movies that will be returned by repository
        val ids = listOf(1, 2)
        val expectedMovies = createMovieList(ids)
        every { repository.getPopularMovies(true) } returns flow {
            emit(Resource.success(expectedMovies))
        }

        // When getting popular movies from use case
        val moviesFlow = useCase(true)

        // Then return popular movies from repository
        moviesFlow.assertCompleteStream(Resource.success(expectedMovies))
    }
}
