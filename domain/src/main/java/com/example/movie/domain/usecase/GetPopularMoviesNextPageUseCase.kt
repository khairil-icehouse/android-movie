package com.example.movie.domain.usecase

import com.example.movie.domain.repository.MoviesRepository

class GetPopularMoviesNextPageUseCase(private val moviesRepository: MoviesRepository) {

    operator fun invoke() = moviesRepository.getPopularMoviesNextPage()
}
