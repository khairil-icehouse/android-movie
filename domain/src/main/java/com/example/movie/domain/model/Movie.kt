package com.example.movie.domain.model

data class Movie(
    val id: Int,
    val title: String,
    val posterUrl: String,
    val overview: String,
    val genres: List<Genre>,
    val voteCount: Int,
    val voteAverage: Double,
    val releaseDate: String,
    val backdropUrl: String
)
