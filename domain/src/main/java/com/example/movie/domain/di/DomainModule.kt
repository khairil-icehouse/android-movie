package com.example.movie.domain.di

import com.example.movie.domain.usecase.GetMovieUseCase
import com.example.movie.domain.usecase.GetNowPlayingMoviesNextPageUseCase
import com.example.movie.domain.usecase.GetNowPlayingMoviesUseCase
import com.example.movie.domain.usecase.GetPopularMoviesNextPageUseCase
import com.example.movie.domain.usecase.GetPopularMoviesUseCase
import com.example.movie.domain.util.GenresNameFiller
import org.koin.dsl.module

val domainModule = module {
    factory { GetPopularMoviesUseCase(get()) }

    factory { GetPopularMoviesNextPageUseCase(get()) }

    factory { GetNowPlayingMoviesUseCase(get(), get()) }

    factory { GetNowPlayingMoviesNextPageUseCase(get()) }

    factory { GetMovieUseCase(get(), get()) }

    single { GenresNameFiller(get()) }
}
