package com.example.movie.domain.usecase

import com.example.movie.domain.repository.MoviesRepository

class GetPopularMoviesUseCase(private val moviesRepository: MoviesRepository) {

    operator fun invoke(forceRefresh: Boolean) = moviesRepository.getPopularMovies(forceRefresh)
}
