package com.example.movie.domain.model

enum class Status {
    SUCCESS, ERROR, LOADING
}
