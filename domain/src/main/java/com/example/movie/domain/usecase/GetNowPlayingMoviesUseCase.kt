package com.example.movie.domain.usecase

import com.example.movie.domain.model.Movie
import com.example.movie.domain.model.Resource
import com.example.movie.domain.repository.MoviesRepository
import com.example.movie.domain.util.GenresNameFiller
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.Flow

@ExperimentalCoroutinesApi
class GetNowPlayingMoviesUseCase(
    private val moviesRepository: MoviesRepository,
    private val genresNameFiller: GenresNameFiller
) {

    operator fun invoke(forceRefresh: Boolean): Flow<Resource<List<Movie>>> {
        return genresNameFiller.fillMovies(moviesRepository.getNowPlayingMovies(forceRefresh))
    }
}
