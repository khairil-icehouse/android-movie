package com.example.movie.domain.repository

import com.example.movie.domain.model.Resource
import kotlinx.coroutines.flow.Flow

interface GenresRepository {

    fun getGenresMapper(): Flow<Resource<Map<Int, String>>>
}
