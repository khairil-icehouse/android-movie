package com.example.movie.domain.usecase

import com.example.movie.domain.repository.MoviesRepository

class GetNowPlayingMoviesNextPageUseCase(private val moviesRepository: MoviesRepository) {

    operator fun invoke() = moviesRepository.getNowPlayingMoviesNextPage()
}
