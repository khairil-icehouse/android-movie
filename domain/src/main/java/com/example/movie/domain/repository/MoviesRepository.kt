package com.example.movie.domain.repository

import com.example.movie.domain.model.Movie
import com.example.movie.domain.model.Resource
import kotlinx.coroutines.flow.Flow

interface MoviesRepository {

    fun getPopularMovies(forceRefresh: Boolean): Flow<Resource<List<Movie>>>

    fun getPopularMoviesNextPage(): Flow<Resource<Unit>>

    fun getNowPlayingMovies(forceRefresh: Boolean): Flow<Resource<List<Movie>>>

    fun getNowPlayingMoviesNextPage(): Flow<Resource<Unit>>

    fun getMovie(movieId: Int): Flow<Movie>
}
