package com.example.movie.domain.util

import com.example.movie.domain.model.Movie
import com.example.movie.domain.model.Resource
import com.example.movie.domain.repository.GenresRepository
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.combine

class GenresNameFiller(private val genresRepository: GenresRepository) {

    @ExperimentalCoroutinesApi
    fun fillMovies(movies: Flow<Resource<List<Movie>>>): Flow<Resource<List<Movie>>> {
        return movies
            .combine(genresRepository.getGenresMapper()) { moviesResource, genresMapperResource ->
                if (genresMapperResource.data == null || moviesResource.data == null) {
                    moviesResource
                } else {
                    val genresMapper = genresMapperResource.data
                    val moviesWithGenres = fill(moviesResource.data, genresMapper)
                    moviesResource.copy(data = moviesWithGenres)
                }
            }
    }

    @ExperimentalCoroutinesApi
    fun fillMovie(movieFlow: Flow<Movie>): Flow<Movie> {
        return movieFlow
            .combine(genresRepository.getGenresMapper()) { movie, genresMapperResource ->
                if (genresMapperResource.data == null) {
                    movie
                } else {
                    val genresMapper = genresMapperResource.data
                    fill(movie, genresMapper)
                }
            }
    }

    private fun fill(movies: List<Movie>, map: Map<Int, String>): List<Movie> {
        return movies.map { fill(it, map) }
    }

    private fun fill(movie: Movie, map: Map<Int, String>): Movie {
        val genres = movie.genres.map { it.copy(name = map[it.id] ?: "") }
        return movie.copy(genres = genres)
    }
}
