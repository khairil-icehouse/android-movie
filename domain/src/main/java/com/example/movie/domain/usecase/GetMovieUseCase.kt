package com.example.movie.domain.usecase

import com.example.movie.domain.model.Movie
import com.example.movie.domain.repository.MoviesRepository
import com.example.movie.domain.util.GenresNameFiller
import kotlinx.coroutines.flow.Flow

class GetMovieUseCase(
    private val moviesRepository: MoviesRepository,
    private val genresNameFiller: GenresNameFiller
) {

    operator fun invoke(movieId: Int): Flow<Movie> {
        return genresNameFiller.fillMovie(moviesRepository.getMovie(movieId))
    }
}
