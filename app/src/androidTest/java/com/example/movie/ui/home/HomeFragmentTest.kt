package com.example.movie.ui.home

import androidx.fragment.app.testing.launchFragmentInContainer
import androidx.lifecycle.MutableLiveData
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.navigation.testing.TestNavHostController
import androidx.recyclerview.widget.RecyclerView
import androidx.test.core.app.ApplicationProvider
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.action.ViewActions.swipeDown
import androidx.test.espresso.assertion.ViewAssertions.doesNotExist
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.contrib.RecyclerViewActions.actionOnItemAtPosition
import androidx.test.espresso.contrib.RecyclerViewActions.scrollToPosition
import androidx.test.espresso.matcher.ViewMatchers.assertThat
import androidx.test.espresso.matcher.ViewMatchers.isDescendantOfA
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.espresso.matcher.ViewMatchers.withText
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.airbnb.epoxy.EpoxyAsyncUtil.MAIN_THREAD_HANDLER
import com.airbnb.epoxy.EpoxyController
import com.example.movie.R
import com.example.movie.androidtest.shared.util.RecyclerViewMatcher.Companion.withIdInRecyclerViewItem
import com.example.movie.androidtest.shared.util.postValueAndSleep
import com.example.movie.domain.model.Movie
import com.example.movie.domain.model.Resource
import com.example.movie.test.shared.util.MovieUtil.fakeNowPlayingMovies
import com.example.movie.test.shared.util.MovieUtil.fakePopularMovies
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.CoreMatchers.allOf
import org.hamcrest.CoreMatchers.not
import org.junit.Before
import org.junit.Ignore
import org.junit.Test
import org.junit.runner.RunWith
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.context.loadKoinModules
import org.koin.dsl.module

@RunWith(AndroidJUnit4::class)
class HomeFragmentTest {

    private lateinit var navController: NavController
    private lateinit var viewModel: HomeViewModel
    private val popularMovies = MutableLiveData<Resource<List<Movie>>?>()
    private val nowPlayingMovies = MutableLiveData<Resource<List<Movie>>?>()
    private val errorMessage = MutableLiveData<String?>()
    private val isLoadingPopularMovies = MutableLiveData<Boolean?>()
    private val isLoadingPopularMoviesNextPage = MutableLiveData<Boolean?>()
    private val isLoadingNowPlayingMovies = MutableLiveData<Boolean?>()
    private val isLoadingNowPlayingMoviesNextPage = MutableLiveData<Boolean?>()

    @Before
    fun setupTest() {
        EpoxyController.defaultDiffingHandler = MAIN_THREAD_HANDLER
        mockViewModel()
        val module = module { viewModel(override = true) { viewModel } }
        loadKoinModules(module)
        navController = TestNavHostController(ApplicationProvider.getApplicationContext())
        navController.setGraph(R.navigation.nav_graph)
        launchFragmentInContainer(themeResId = R.style.Theme_AppTheme) { HomeFragment() }.apply {
            onFragment { fragment ->
                Navigation.setViewNavController(fragment.requireView(), navController)
            }
        }
    }

    private fun mockViewModel() {
        viewModel = mockk(relaxed = true)
        every { viewModel.refresh() } answers {}
        every { viewModel.popularMovies } returns popularMovies
        every { viewModel.nowPlayingMovies } returns nowPlayingMovies
        every { viewModel.errorMessage } returns errorMessage
        every { viewModel.isLoadingPopularMovies } returns isLoadingPopularMovies
        every { viewModel.isLoadingPopularMoviesNextPage } returns isLoadingPopularMoviesNextPage
        every { viewModel.isLoadingPopularMoviesNextPage } returns isLoadingPopularMoviesNextPage
        every { viewModel.isLoadingNowPlayingMovies } returns isLoadingNowPlayingMovies
        every { viewModel.isLoadingNowPlayingMoviesNextPage } returns
            isLoadingNowPlayingMoviesNextPage
    }

    @Test
    fun progressBarIsShownWhenLoadingPopularMovies() {
        // When loading popular movies
        isLoadingPopularMovies.postValueAndSleep(true)
        // Then progress bar is visible
        onView(withIdInRecyclerViewItem(R.id.home_rv, 0, R.id.title_progress_bar))
            .check(matches(isDisplayed()))

        // When loading popular movies is finished
        isLoadingPopularMovies.postValueAndSleep(false)
        // Then progress bar is gone
        onView(withIdInRecyclerViewItem(R.id.home_rv, 0, R.id.title_progress_bar))
            .check(matches(not(isDisplayed())))
    }

    @Test
    fun progressBarIsShownWhenLoadingNowPlayingMovies() {
        // When loading now playing movies
        isLoadingNowPlayingMovies.postValueAndSleep(true)
        // Then progress bar is visible
        onView(withIdInRecyclerViewItem(R.id.home_rv, 2, R.id.title_progress_bar))
            .check(matches(isDisplayed()))

        // When loading now playing movies is finished
        isLoadingNowPlayingMovies.postValueAndSleep(false)
        // Then progress bar is gone
        onView(withIdInRecyclerViewItem(R.id.home_rv, 2, R.id.title_progress_bar))
            .check(matches(not(isDisplayed())))
    }

    @Test
    fun moviesRefreshAfterSwipeRefreshIsTriggered() {
        // When swipe refresh layout is pulled down
        onView(withId(R.id.swipe_refresh)).perform(swipeDown())
        // Then refresh
        verify { viewModel.refresh() }
    }

    @Test
    fun popularMoviesIsShownAfterLoaded() {
        // When popular movies is loaded
        popularMovies.postValueAndSleep(Resource.success(fakePopularMovies))
        // popular movies is displayed on recycler view
        val movieRv = withIdInRecyclerViewItem(R.id.home_rv, 1, R.id.movie_list)
        fakePopularMovies.forEachIndexed { index, movie ->
            onView(movieRv).perform(scrollToPosition<RecyclerView.ViewHolder>(index))
            onView(
                allOf(
                    isDescendantOfA(movieRv),
                    withIdInRecyclerViewItem(R.id.movie_list, index, R.id.title)
                )
            ).check(matches(withText(movie.title)))
        }
    }

    @Test
    fun nowPlayingMoviesIsShownAfterLoaded() {
        // When now playing movies is loaded
        nowPlayingMovies.postValueAndSleep(Resource.success(fakeNowPlayingMovies))
        // now playing movies is displayed on recycler view
        fakeNowPlayingMovies.forEachIndexed { index, movie ->
            val recyclerViewIndex = index + 3
            onView(withId(R.id.home_rv))
                .perform(scrollToPosition<RecyclerView.ViewHolder>(recyclerViewIndex))
            onView(withIdInRecyclerViewItem(R.id.home_rv, recyclerViewIndex, R.id.title))
                .check(matches(withText(movie.title)))
        }
    }

    @Ignore("InfiniteScrollListener is not triggered")
    @Test
    fun scrollToLastPopularMovieTriggersLoadMore() {
        // Given fragment with loaded popular movies
        popularMovies.postValueAndSleep(Resource.success(fakePopularMovies))
        // When scrolling to the last popular movie
        onView(withIdInRecyclerViewItem(R.id.home_rv, 1, R.id.movie_list))
            .perform(scrollToPosition<RecyclerView.ViewHolder>(fakePopularMovies.size - 1))
        // Then load popular movies next page is called
        verify { viewModel.loadPopularMoviesNextPage() }
    }

    @Ignore("InfiniteScrollListener is not triggered")
    @Test
    fun scrollToLastNowPlayingMovieTriggersLoadMore() {
        // Given fragment with loaded now playing movies
        nowPlayingMovies.postValueAndSleep(Resource.success(fakeNowPlayingMovies))
        // When scrolling to the last now playing movies
        onView(withId(R.id.home_rv))
            .perform(scrollToPosition<RecyclerView.ViewHolder>(3 + fakeNowPlayingMovies.size - 1))
        // Then load now playing movies next page is called
        verify { viewModel.loadNowPlayingMoviesNextPage() }
    }

    @Test
    fun progressBarIsShownWhenLoadingMorePopularMovies() {
        // When loading popular movies next page
        isLoadingPopularMoviesNextPage.postValueAndSleep(true)
        // Then progress bar is visible
        onView(withIdInRecyclerViewItem(R.id.home_rv, 0, R.id.title_progress_bar))
            .check(matches(isDisplayed()))

        // When loading popular movies next page is finished
        isLoadingPopularMoviesNextPage.postValueAndSleep(false)
        // Then progress bar is gone
        onView(withIdInRecyclerViewItem(R.id.home_rv, 0, R.id.title_progress_bar))
            .check(matches(not(isDisplayed())))
    }

    @Test
    fun progressBarIsShownWhenLoadingMoreNowPlayingMovies() {
        // When loading now playing movies next page
        isLoadingNowPlayingMoviesNextPage.postValueAndSleep(true)
        // Then progress bar is visible
        onView(withId(R.id.progress_bar)).check(matches(isDisplayed()))

        // When loading now playing movies next page is finished
        isLoadingNowPlayingMoviesNextPage.postValueAndSleep(false)
        // Then progress bar is gone
        onView(withId(R.id.progress_bar)).check(doesNotExist())
    }

    @Test
    fun popularMovieItemClickedNavigateToMovieDetail() {
        // Given fragment with loaded popular movies
        popularMovies.postValueAndSleep(Resource.success(fakePopularMovies))
        // When a popular movie is clicked
        onView(withId(R.id.movie_list))
            .perform(actionOnItemAtPosition<RecyclerView.ViewHolder>(0, click()))
        // Then navigate to movie detail
        assertThat(navController.currentDestination?.id, `is`(R.id.movie_detail_dest))
    }

    @Test
    fun nowPlayingMovieItemClickedNavigateToMovieDetail() {
        // Given fragment with loaded now playing movies
        nowPlayingMovies.postValueAndSleep(Resource.success(fakeNowPlayingMovies))
        // When a now playing is clicked
        onView(withId(R.id.home_rv))
            .perform(actionOnItemAtPosition<RecyclerView.ViewHolder>(3, click()))
        // Then navigate to movie detail
        assertThat(navController.currentDestination?.id, `is`(R.id.movie_detail_dest))
    }
}
