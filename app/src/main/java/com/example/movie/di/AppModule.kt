package com.example.movie.di

import com.example.movie.ui.home.HomeViewModel
import com.example.movie.ui.home.NowPlayingMoviesViewModel
import com.example.movie.ui.home.PopularMoviesViewModel
import kotlinx.coroutines.Dispatchers
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.qualifier.named
import org.koin.dsl.module

const val IO_DISPATCHER = "ioDispatcher"
const val UI_DISPATCHER = "uiDispatcher"

val appModule = module {
    viewModel { HomeViewModel(get(), get()) }

    viewModel { PopularMoviesViewModel(get(), get(), get(named(IO_DISPATCHER))) }

    viewModel { NowPlayingMoviesViewModel(get(), get(), get(named(IO_DISPATCHER))) }

    single(named(IO_DISPATCHER)) { Dispatchers.IO }

    single(named(UI_DISPATCHER)) { Dispatchers.Main }
}
