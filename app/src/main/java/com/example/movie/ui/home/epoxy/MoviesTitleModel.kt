package com.example.movie.ui.home.epoxy

import androidx.core.view.isVisible
import com.airbnb.epoxy.EpoxyAttribute
import com.airbnb.epoxy.EpoxyModelClass
import com.example.movie.R
import com.example.movie.databinding.ItemMoviesTitleBinding
import com.example.movie.util.epoxy.ViewBindingEpoxyModelWithHolder

@EpoxyModelClass(layout = R.layout.item_movies_title)
abstract class MoviesTitleModel : ViewBindingEpoxyModelWithHolder<ItemMoviesTitleBinding>() {

    @EpoxyAttribute
    lateinit var title: String

    @EpoxyAttribute
    open var isLoading: Boolean = false

    override fun ItemMoviesTitleBinding.bind() {
        moviesTitle.text = title
        titleProgressBar.isVisible = isLoading
    }
}
