package com.example.movie.ui.home

import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.view.doOnPreDraw
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.fragment.FragmentNavigatorExtras
import androidx.navigation.fragment.findNavController
import com.example.movie.databinding.FragmentHomeBinding
import com.example.movie.domain.model.Movie
import com.example.movie.ui.home.epoxy.HomeEpoxyController
import com.example.movie.util.RecyclerInfiniteScrollListener
import org.koin.androidx.viewmodel.ext.android.viewModel

class HomeFragment : Fragment() {

    private lateinit var binding: FragmentHomeBinding

    private val homeViewModel: HomeViewModel by viewModel()

    private val controller: HomeEpoxyController by lazy { HomeEpoxyController(requireContext()) }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentHomeBinding.inflate(layoutInflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupEnterTransition()
        setupRecyclerView(savedInstanceState)
        setupSwipeRefresh()
        subscribeMovieList()
        subscribeError()
        subscribeLoading()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        controller.onSaveInstanceState(outState)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        binding.homeRv.clear()
        controller.clear()
    }

    private fun setupEnterTransition() {
        postponeEnterTransition()
        binding.root.doOnPreDraw { startPostponedEnterTransition() }
    }

    private fun setupRecyclerView(savedInstanceState: Bundle?) {
        controller.apply {
            onRestoreInstanceState(savedInstanceState)
            popularMoviesInfiniteScrollListener = object : RecyclerInfiniteScrollListener() {
                override fun onLoadMore() {
                    homeViewModel.loadPopularMoviesNextPage()
                }

                override fun isDataLoading(): Boolean {
                    return homeViewModel.isLoadingPopularMoviesNextPage.value ?: false
                }
            }
            callbacks = object : HomeEpoxyController.Callbacks {
                override fun onMovieItemClicked(movie: Movie, view: View) {
                    navigateToMovieDetail(movie, view)
                }
            }
        }
        binding.homeRv.apply {
            setController(controller)
            setItemSpacingDp(8)
            addOnScrollListener(object : RecyclerInfiniteScrollListener() {
                override fun onLoadMore() {
                    homeViewModel.loadNowPlayingMoviesNextPage()
                }

                override fun isDataLoading(): Boolean {
                    return homeViewModel.isLoadingNowPlayingMoviesNextPage.value ?: false
                }
            })
        }
    }

    private fun navigateToMovieDetail(movie: Movie, view: View) {
        val transitionName = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            view.transitionName
        } else {
            ""
        }
        val extras = FragmentNavigatorExtras(view to transitionName)
        val direction = HomeFragmentDirections.actionHomeDestToMovieDetailDest(
            movie.id,
            transitionName
        )
        findNavController().navigate(direction, extras)
    }

    private fun setupSwipeRefresh() {
        binding.swipeRefresh.apply {
            setOnRefreshListener {
                postOnAnimation { isRefreshing = false }
                homeViewModel.refresh()
            }
        }
    }

    private fun subscribeMovieList() {
        homeViewModel.popularMovies.observe(viewLifecycleOwner, Observer {
            it?.let { controller.popularMovies = it.data ?: emptyList() }
        })

        homeViewModel.nowPlayingMovies.observe(viewLifecycleOwner, Observer {
            it?.let { controller.nowPlayingMovies = it.data ?: emptyList() }
        })
    }

    private fun subscribeError() {
        homeViewModel.errorMessage.observe(viewLifecycleOwner, Observer {
            it?.let { Toast.makeText(context, it, Toast.LENGTH_SHORT).show() }
        })
    }

    private fun subscribeLoading() {
        homeViewModel.isLoadingPopularMovies.observe(viewLifecycleOwner, Observer {
            it?.let { controller.isLoadingPopularMovies = it }
        })

        homeViewModel.isLoadingNowPlayingMovies.observe(viewLifecycleOwner, Observer {
            it?.let { controller.isLoadingNowPlayingMovies = it }
        })

        homeViewModel.isLoadingNowPlayingMoviesNextPage.observe(viewLifecycleOwner, Observer {
            it?.let { controller.isLoadingNowPlayingMoviesNextPage = it }
        })

        homeViewModel.isLoadingPopularMoviesNextPage.observe(viewLifecycleOwner, Observer {
            it?.let { controller.isLoadingPopularMoviesNextPage = it }
        })
    }
}
