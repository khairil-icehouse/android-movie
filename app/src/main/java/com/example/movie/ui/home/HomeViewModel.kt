package com.example.movie.ui.home

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.ViewModel

class HomeViewModel(
    private val popularMoviesViewModel: PopularMoviesViewModel,
    private val nowPlayingMoviesViewModel: NowPlayingMoviesViewModel
) : ViewModel() {

    val popularMovies = popularMoviesViewModel.movies

    val nowPlayingMovies = nowPlayingMoviesViewModel.movies

    private val _errorMessage = MediatorLiveData<String?>()
    val errorMessage: LiveData<String?>
        get() = _errorMessage

    val isLoadingPopularMovies: LiveData<Boolean?>
        get() = popularMoviesViewModel.isLoadingMovies

    val isLoadingPopularMoviesNextPage: LiveData<Boolean?>
        get() = popularMoviesViewModel.isLoadingMoviesNextPage

    val isLoadingNowPlayingMovies: LiveData<Boolean?>
        get() = nowPlayingMoviesViewModel.isLoadingMovies

    val isLoadingNowPlayingMoviesNextPage: LiveData<Boolean?>
        get() = nowPlayingMoviesViewModel.isLoadingMoviesNextPage

    init {
        setupErrorMessageLiveData()
    }

    fun refresh() {
        popularMoviesViewModel.refresh()
        nowPlayingMoviesViewModel.refresh()
    }

    fun loadPopularMoviesNextPage() {
        popularMoviesViewModel.loadNextPage()
    }

    fun loadNowPlayingMoviesNextPage() {
        nowPlayingMoviesViewModel.loadNextPage()
    }

    private fun setupErrorMessageLiveData() {
        _errorMessage.addSource(popularMoviesViewModel.errorMessage) { _errorMessage.value = it }
        _errorMessage.addSource(nowPlayingMoviesViewModel.errorMessage) { _errorMessage.value = it }
    }
}
