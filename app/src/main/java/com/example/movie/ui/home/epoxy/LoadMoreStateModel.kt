package com.example.movie.ui.home.epoxy

import android.view.View
import com.airbnb.epoxy.EpoxyModel
import com.airbnb.epoxy.EpoxyModelClass
import com.example.movie.R

@EpoxyModelClass(layout = R.layout.item_load_more_state)
abstract class LoadMoreStateModel : EpoxyModel<View>()
