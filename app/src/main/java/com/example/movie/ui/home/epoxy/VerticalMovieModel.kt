package com.example.movie.ui.home.epoxy

import android.os.Build
import coil.api.load
import com.airbnb.epoxy.EpoxyAttribute
import com.airbnb.epoxy.EpoxyModelClass
import com.example.movie.R
import com.example.movie.databinding.ItemVerticalMovieBinding
import com.example.movie.domain.model.Movie
import com.example.movie.util.epoxy.ViewBindingEpoxyModelWithHolder
import com.example.movie.util.setFormattedReleaseDate
import com.example.movie.util.setRatingCountBracket
import com.example.movie.util.setTenRatingScale
import com.example.movie.util.toDisplayNames

@EpoxyModelClass(layout = R.layout.item_vertical_movie)
abstract class VerticalMovieModel : ViewBindingEpoxyModelWithHolder<ItemVerticalMovieBinding>() {

    @EpoxyAttribute
    lateinit var movie: Movie

    @EpoxyAttribute
    var callbacks: HomeEpoxyController.Callbacks? = null

    override fun ItemVerticalMovieBinding.bind() {
        title.text = movie.title
        image.load(movie.posterUrl)
        genres.text = movie.genres.toDisplayNames()
        rating.setTenRatingScale(movie.voteAverage)
        ratingCount.setRatingCountBracket(movie.voteCount)
        releaseDate.setFormattedReleaseDate(movie.releaseDate)
        root.setOnClickListener { callbacks?.onMovieItemClicked(movie, imageCardView) }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            imageCardView.transitionName = "vertical_${movie.id}"
        }
    }
}
