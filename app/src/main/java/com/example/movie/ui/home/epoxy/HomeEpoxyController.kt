package com.example.movie.ui.home.epoxy

import android.content.Context
import android.view.View
import com.airbnb.epoxy.EpoxyController
import com.example.movie.R
import com.example.movie.domain.model.Movie
import com.example.movie.util.RecyclerInfiniteScrollListener
import com.example.movie.util.epoxy.EpoxyModelProperty

class HomeEpoxyController(private val context: Context) : EpoxyController() {

    interface Callbacks {
        fun onMovieItemClicked(movie: Movie, view: View)
    }

    var callbacks: Callbacks? = null

    var nowPlayingMovies by EpoxyModelProperty { emptyList<Movie>() }
    var popularMovies by EpoxyModelProperty { emptyList<Movie>() }
    var isLoadingPopularMovies by EpoxyModelProperty { false }
    var isLoadingPopularMoviesNextPage by EpoxyModelProperty { false }
    var isLoadingNowPlayingMovies by EpoxyModelProperty { false }
    var isLoadingNowPlayingMoviesNextPage by EpoxyModelProperty { false }

    lateinit var popularMoviesInfiniteScrollListener: RecyclerInfiniteScrollListener

    override fun buildModels() {
        moviesTitle {
            id("popularMoviesTitle")
            title(context.resources.getString(R.string.popular_movies))
            isLoading(isLoadingPopularMovies || isLoadingPopularMoviesNextPage)
        }
        horizontalMovieList {
            id("popularMovies")
            movies(popularMovies)
            scrollListener(popularMoviesInfiniteScrollListener)
            callbacks(callbacks)
        }
        moviesTitle {
            id("nowPlayingMoviesTitle")
            title(context.resources.getString(R.string.now_playing_movies))
            isLoading(isLoadingNowPlayingMovies)
        }
        nowPlayingMovies.forEach {
            verticalMovie {
                id(it.id)
                movie(it)
                callbacks(callbacks)
            }
        }
        if (isLoadingNowPlayingMoviesNextPage) {
            loadMoreState {
                id("nowPlayingMoviesLoadMoreState")
            }
        }
    }

    fun clear() {
        callbacks = null
    }
}
