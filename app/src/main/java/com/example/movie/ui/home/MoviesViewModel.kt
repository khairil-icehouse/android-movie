package com.example.movie.ui.home

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import androidx.lifecycle.map
import com.example.movie.domain.model.Movie
import com.example.movie.domain.model.Resource
import com.example.movie.domain.model.Status
import com.example.movie.domain.usecase.GetNowPlayingMoviesNextPageUseCase
import com.example.movie.domain.usecase.GetNowPlayingMoviesUseCase
import com.example.movie.domain.usecase.GetPopularMoviesNextPageUseCase
import com.example.movie.domain.usecase.GetPopularMoviesUseCase
import com.example.movie.util.SwitchableLiveData
import kotlinx.coroutines.CoroutineDispatcher

abstract class MoviesViewModel : ViewModel() {

    private val _movies = SwitchableLiveData<Resource<List<Movie>>?>()
    val movies: LiveData<Resource<List<Movie>>?>
        get() = _movies

    val isLoadingMovies: LiveData<Boolean?> = movies.map {
        it?.status == Status.LOADING
    }

    private val moviesErrorMessage: LiveData<String?> = movies.map {
        it?.message
    }

    private val loadingNextPageState = SwitchableLiveData<Resource<Unit>?>()

    val isLoadingMoviesNextPage: LiveData<Boolean?> = loadingNextPageState.map {
        it?.status == Status.LOADING
    }

    private val moviesNextPageErrorMessage = loadingNextPageState.map {
        it?.message
    }

    private val _errorMessage = MediatorLiveData<String?>()
    val errorMessage: LiveData<String?>
        get() = _errorMessage

    init {
        setupErrorMessageLiveData()
    }

    fun refresh() {
        _movies.switchSource(getMoviesLiveData(true))
    }

    fun loadNextPage() {
        loadingNextPageState.switchSource(getLoadingNextPageStateLiveData())
    }

    protected fun setupMoviesLiveData() {
        _movies.switchSource(getMoviesLiveData(false))
    }

    private fun setupErrorMessageLiveData() {
        _errorMessage.addSource(moviesErrorMessage) { _errorMessage.value = it }
        _errorMessage.addSource(moviesNextPageErrorMessage) { _errorMessage.value = it }
    }

    abstract fun getMoviesLiveData(forceRefresh: Boolean): LiveData<Resource<List<Movie>>?>

    abstract fun getLoadingNextPageStateLiveData(): LiveData<Resource<Unit>?>
}

class PopularMoviesViewModel(
    private val getPopularMoviesUseCase: GetPopularMoviesUseCase,
    private val getPopularMoviesNextPageUseCase: GetPopularMoviesNextPageUseCase,
    private val ioDispatcher: CoroutineDispatcher
) : MoviesViewModel() {

    init {
        super.setupMoviesLiveData()
    }

    override fun getMoviesLiveData(forceRefresh: Boolean): LiveData<Resource<List<Movie>>?> {
        return getPopularMoviesUseCase(forceRefresh).asLiveData(ioDispatcher)
    }

    override fun getLoadingNextPageStateLiveData(): LiveData<Resource<Unit>?> {
        return getPopularMoviesNextPageUseCase().asLiveData(ioDispatcher)
    }
}

class NowPlayingMoviesViewModel(
    private val getNowPlayingMoviesUseCase: GetNowPlayingMoviesUseCase,
    private val getNowPlayingMoviesNextPageUseCase: GetNowPlayingMoviesNextPageUseCase,
    private val ioDispatcher: CoroutineDispatcher
) : MoviesViewModel() {

    init {
        super.setupMoviesLiveData()
    }

    override fun getMoviesLiveData(forceRefresh: Boolean): LiveData<Resource<List<Movie>>?> {
        return getNowPlayingMoviesUseCase(forceRefresh).asLiveData(ioDispatcher)
    }

    override fun getLoadingNextPageStateLiveData(): LiveData<Resource<Unit>?> {
        return getNowPlayingMoviesNextPageUseCase().asLiveData(ioDispatcher)
    }
}
