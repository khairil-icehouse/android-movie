package com.example.movie.ui.home.epoxy

import android.os.Build
import coil.api.load
import com.airbnb.epoxy.EpoxyAttribute
import com.airbnb.epoxy.EpoxyModelClass
import com.example.movie.R
import com.example.movie.databinding.ItemHorizontalMovieBinding
import com.example.movie.domain.model.Movie
import com.example.movie.util.epoxy.ViewBindingEpoxyModelWithHolder
import com.example.movie.util.setRatingCountBracket
import com.example.movie.util.setTenRatingScale

@EpoxyModelClass(layout = R.layout.item_horizontal_movie)
abstract class HorizontalMovieModel :
    ViewBindingEpoxyModelWithHolder<ItemHorizontalMovieBinding>() {

    @EpoxyAttribute
    lateinit var movie: Movie

    @EpoxyAttribute
    var callbacks: HomeEpoxyController.Callbacks? = null

    override fun ItemHorizontalMovieBinding.bind() {
        title.text = movie.title
        image.load(movie.posterUrl)
        rating.setTenRatingScale(movie.voteAverage)
        ratingCount.setRatingCountBracket(movie.voteCount)
        root.setOnClickListener { callbacks?.onMovieItemClicked(movie, imageCardView) }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            imageCardView.transitionName = "horizontal_${movie.id}"
        }
    }
}
