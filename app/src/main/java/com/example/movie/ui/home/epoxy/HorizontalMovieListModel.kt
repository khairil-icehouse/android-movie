package com.example.movie.ui.home.epoxy

import com.airbnb.epoxy.EpoxyAttribute
import com.airbnb.epoxy.EpoxyModelClass
import com.example.movie.R
import com.example.movie.databinding.ItemHorizontalMovieListBinding
import com.example.movie.domain.model.Movie
import com.example.movie.util.RecyclerInfiniteScrollListener
import com.example.movie.util.epoxy.ViewBindingEpoxyModelWithHolder

@EpoxyModelClass(layout = R.layout.item_horizontal_movie_list)
abstract class HorizontalMovieListModel :
    ViewBindingEpoxyModelWithHolder<ItemHorizontalMovieListBinding>() {

    @EpoxyAttribute
    lateinit var movies: List<Movie>

    @EpoxyAttribute
    lateinit var scrollListener: RecyclerInfiniteScrollListener

    @EpoxyAttribute
    var callbacks: HomeEpoxyController.Callbacks? = null

    override fun shouldSaveViewState(): Boolean {
        return true
    }

    override fun ItemHorizontalMovieListBinding.bind() {
        movieList.setItemSpacingDp(8)
        val models = movies.map {
            HorizontalMovieModel_()
                .id(it.id)
                .movie(it)
                .callbacks(callbacks)
        }
        movieList.setModels(models)
        movieList.addOnScrollListener(scrollListener)
    }
}
