package com.example.movie.util

import androidx.core.widget.NestedScrollView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

interface InfiniteScrollListener {

    fun onLoadMore()

    fun isDataLoading(): Boolean
}

abstract class RecyclerInfiniteScrollListener : RecyclerView.OnScrollListener(),
    InfiniteScrollListener {

    private val loadMoreRunnable = Runnable { onLoadMore() }

    override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
        if (dx < 0 || dy < 0 || (dx <= 0 && dy <= 0) || isDataLoading()) return

        val layoutManager = recyclerView.layoutManager as? LinearLayoutManager ?: return

        val totalItemCount = layoutManager.itemCount
        val lastVisibleItem = layoutManager.findLastVisibleItemPosition()
        val firstVisibleItem = layoutManager.findFirstVisibleItemPosition()
        val visibleItemCount = lastVisibleItem - firstVisibleItem + 1

        if (lastVisibleItem >= totalItemCount - 2 && totalItemCount > visibleItemCount) {
            recyclerView.post(loadMoreRunnable)
        }
    }
}

abstract class NestedScrollInfiniteScrollListener : NestedScrollView.OnScrollChangeListener,
    InfiniteScrollListener {

    override fun onScrollChange(
        v: NestedScrollView?,
        scrollX: Int,
        scrollY: Int,
        oldScrollX: Int,
        oldScrollY: Int
    ) {
        v?.let {
            val lastChild = getLastChild(v)
            if (lastChild != null &&
                (scrollY >= (lastChild.measuredHeight - v.measuredHeight)) &&
                scrollY > oldScrollY && !isDataLoading()
            ) {
                onLoadMore()
            }
        }
    }

    private fun getLastChild(v: NestedScrollView) = v.getChildAt(v.childCount - 1)
}
