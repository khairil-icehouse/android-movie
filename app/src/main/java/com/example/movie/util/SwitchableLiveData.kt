package com.example.movie.util

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData

class SwitchableLiveData<T> : MediatorLiveData<T>() {

    private var currentSource: LiveData<T>? = null

    fun switchSource(newSource: LiveData<T>) {
        removeCurrentSource()
        currentSource = newSource
        addSource(newSource) {
            value = it
        }
    }

    private fun removeCurrentSource() {
        val oldSource = currentSource
        if (oldSource != null) {
            removeSource(oldSource)
            currentSource = null
        }
    }
}
