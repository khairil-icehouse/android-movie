package com.example.movie.util

import android.widget.RatingBar
import android.widget.TextView
import com.example.movie.domain.model.Genre
import java.text.SimpleDateFormat

fun List<Genre>.toDisplayNames(): String {
    return filter { it.name.isNotEmpty() }.joinToString(prefix = "", postfix = "") { it.name }
}

fun TextView.setGenres(genres: List<Genre>) {
    text = genres.toDisplayNames()
}

fun TextView.setRatingCount(voteCount: Int) {
    text = "$voteCount votes"
}

fun TextView.setRatingCountBracket(voteCount: Int) {
    text = "($voteCount)"
}

fun RatingBar.setTenRatingScale(voteAverage: Double) {
    rating = (voteAverage / 2).toFloat()
}

fun TextView.setRatingPercentage(voteAverage: Double) {
    text = "%.0f%%".format(voteAverage * 10)
}

fun TextView.setFormattedReleaseDate(releaseDate: String) {
    text = try {
        val date = SimpleDateFormat("yyyy-MM-dd").parse(releaseDate)
        SimpleDateFormat("dd MMMM yyyy").format(date)
    } catch (e: Exception) {
        ""
    }
}
