package com.example.movie

import android.app.Application
import com.example.movie.data.di.dataModule
import com.example.movie.di.appModule
import com.example.movie.domain.di.domainModule
import com.example.movie.util.epoxy.setupEpoxy
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin
import timber.log.Timber

class MovieApp : Application() {
    override fun onCreate() {
        super.onCreate()
        setupTimber()
        setupDependencies()
        setupEpoxy()
    }

    private fun setupTimber() {
        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }
    }

    private fun setupDependencies() {
        startKoin {
            androidContext(this@MovieApp)
            modules(
                listOf(
                    appModule, domainModule, dataModule
                )
            )
        }
    }
}
