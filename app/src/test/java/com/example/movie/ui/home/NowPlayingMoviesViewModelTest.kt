package com.example.movie.ui.home

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.example.movie.androidtest.shared.util.getOrAwaitValue
import com.example.movie.di.setupDependencies
import com.example.movie.domain.model.Resource
import com.example.movie.test.shared.fake.FakeMoviesRepository
import com.example.movie.test.shared.util.GenreUtil.fillGenresName
import com.example.movie.test.shared.util.MovieUtil.fakeNowPlayingMovies
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.setMain
import org.hamcrest.CoreMatchers.`is`
import org.junit.After
import org.junit.Assert.assertThat
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.koin.core.context.stopKoin
import org.koin.core.parameter.parametersOf
import org.koin.test.KoinTest
import org.koin.test.get

class NowPlayingMoviesViewModelTest : KoinTest {

    private lateinit var viewModel: NowPlayingMoviesViewModel

    private val dispatcher = Dispatchers.Unconfined

    @get:Rule
    var instantExecutorRule = InstantTaskExecutorRule()

    @Before
    fun setupTest() {
        setupDependencies()
        Dispatchers.setMain(dispatcher)
    }

    @After
    fun tearDownTest() {
        stopKoin()
    }

    private fun provideViewModel(isSuccess: Boolean = true): NowPlayingMoviesViewModel {
        return get { parametersOf(isSuccess, dispatcher) }
    }

    @Test
    fun `get movies at start`() = runBlocking {
        // Given view model
        viewModel = provideViewModel()

        // When view model is created

        // Then view model is loading data from repository
        assertThat(viewModel.isLoadingMovies.getOrAwaitValue(), `is`(true))
        assertThat(viewModel.movies.value, `is`(Resource.loading()))
        // Then successful data is returned
        assertThat(viewModel.isLoadingMovies.getOrAwaitValue(), `is`(false))
        assertThat(
            viewModel.movies.value,
            `is`(Resource.success(fakeNowPlayingMovies.fillGenresName()))
        )
    }

    @Test
    fun `refresh movies`() = runBlocking {
        // Given view model that has loaded its first data
        viewModel = provideViewModel()
        viewModel.isLoadingMovies.getOrAwaitValue()
        viewModel.isLoadingMovies.getOrAwaitValue()

        // When refreshing movies
        viewModel.refresh()

        // Then view model loads new data
        assertThat(viewModel.isLoadingMovies.getOrAwaitValue(), `is`(true))
        assertThat(viewModel.movies.value, `is`(Resource.loading()))
        assertThat(viewModel.isLoadingMovies.getOrAwaitValue(), `is`(false))
        assertThat(
            viewModel.movies.value,
            `is`(Resource.success(fakeNowPlayingMovies.fillGenresName()))
        )
    }

    @Test
    fun `load next page`() = runBlocking {
        // Given view model that has loaded its first data
        viewModel = provideViewModel()
        viewModel.isLoadingMovies.getOrAwaitValue()
        viewModel.isLoadingMovies.getOrAwaitValue()

        // When loading next page
        viewModel.loadNextPage()

        // Then view model loads next page
        assertThat(viewModel.isLoadingMoviesNextPage.getOrAwaitValue(), `is`(true))
        assertThat(viewModel.isLoadingMoviesNextPage.getOrAwaitValue(), `is`(false))
    }

    @Test
    fun `set error message value from repository`() = runBlocking {
        // Given view model with repository that always return error
        viewModel = provideViewModel(false)

        // When view model is done loading first data
        viewModel.isLoadingMovies.getOrAwaitValue()

        // Then error message value is set
        assertThat(
            viewModel.errorMessage.getOrAwaitValue(),
            `is`(FakeMoviesRepository.errorMessage)
        )
    }
}
