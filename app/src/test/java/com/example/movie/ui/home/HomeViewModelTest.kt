package com.example.movie.ui.home

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.example.movie.androidtest.shared.util.getOrAwaitValue
import com.example.movie.di.setupDependencies
import com.example.movie.domain.model.Resource
import com.example.movie.test.shared.fake.FakeMoviesRepository
import com.example.movie.test.shared.util.GenreUtil.fillGenresName
import com.example.movie.test.shared.util.MovieUtil.fakeNowPlayingMovies
import com.example.movie.test.shared.util.MovieUtil.fakePopularMovies
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.setMain
import org.hamcrest.CoreMatchers.`is`
import org.junit.After
import org.junit.Assert.assertThat
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.koin.core.context.stopKoin
import org.koin.core.parameter.parametersOf
import org.koin.test.KoinTest
import org.koin.test.get

@ExperimentalCoroutinesApi
class HomeViewModelTest : KoinTest {

    private lateinit var viewModel: HomeViewModel

    private val dispatcher = Dispatchers.Unconfined

    @get:Rule
    var instantExecutorRule = InstantTaskExecutorRule()

    @Before
    fun setupTest() {
        setupDependencies()
        Dispatchers.setMain(dispatcher)
    }

    @After
    fun tearDownTest() {
        stopKoin()
    }

    private fun provideViewModel(isSuccess: Boolean = true): HomeViewModel {
        return get { parametersOf(isSuccess, dispatcher) }
    }

    @Test
    fun `load popular and now playing movies at start`() = runBlocking {
        // Given view model
        viewModel = provideViewModel()

        // When view model is created

        // Then load popular and now playing movies
        assertThat(viewModel.isLoadingPopularMovies.getOrAwaitValue(), `is`(true))
        assertThat(viewModel.isLoadingNowPlayingMovies.getOrAwaitValue(), `is`(true))
        assertThat(viewModel.isLoadingPopularMovies.getOrAwaitValue(), `is`(false))
        assertThat(viewModel.isLoadingNowPlayingMovies.getOrAwaitValue(), `is`(false))
        assertThat(viewModel.popularMovies.value, `is`(Resource.success(fakePopularMovies)))
        assertThat(
            viewModel.nowPlayingMovies.value,
            `is`(Resource.success(fakeNowPlayingMovies.fillGenresName()))
        )
    }

    @Test
    fun `refresh reloads popular and now playing movies`() = runBlocking {
        // Given view model that has loaded its data
        viewModel = provideViewModel()
        viewModel.isLoadingPopularMovies.getOrAwaitValue()
        viewModel.isLoadingPopularMovies.getOrAwaitValue()

        // When refreshing view model
        viewModel.refresh()

        // Then popular and now playing movies is reloaded
        assertThat(viewModel.isLoadingPopularMovies.getOrAwaitValue(), `is`(true))
        assertThat(viewModel.isLoadingNowPlayingMovies.getOrAwaitValue(), `is`(true))
        assertThat(viewModel.isLoadingPopularMovies.getOrAwaitValue(), `is`(false))
        assertThat(viewModel.isLoadingNowPlayingMovies.getOrAwaitValue(), `is`(false))
        assertThat(viewModel.popularMovies.value, `is`(Resource.success(fakePopularMovies)))
        assertThat(
            viewModel.nowPlayingMovies.value,
            `is`(Resource.success(fakeNowPlayingMovies.fillGenresName()))
        )
    }

    @Test
    fun `load next page`() = runBlocking {
        // Given view model that has loaded its data
        viewModel = provideViewModel()
        viewModel.isLoadingPopularMovies.getOrAwaitValue()
        viewModel.isLoadingPopularMovies.getOrAwaitValue()

        // When loading popular movies next page
        viewModel.loadPopularMoviesNextPage()

        // Then view model loads next page
        assertThat(viewModel.isLoadingPopularMoviesNextPage.getOrAwaitValue(), `is`(true))
        assertThat(viewModel.isLoadingPopularMoviesNextPage.getOrAwaitValue(), `is`(false))

        // When loading now playing movies next page
        viewModel.loadNowPlayingMoviesNextPage()

        // Then view model loads next page
        assertThat(viewModel.isLoadingNowPlayingMoviesNextPage.getOrAwaitValue(), `is`(true))
        assertThat(viewModel.isLoadingNowPlayingMoviesNextPage.getOrAwaitValue(), `is`(false))
    }

    @Test
    fun `set error message value from repository`() = runBlocking {
        // Given view model with repository that always return error
        viewModel = provideViewModel(false)

        // When view model is done loading first data
        viewModel.isLoadingPopularMovies.getOrAwaitValue()

        // Then error message value is set
        assertThat(
            viewModel.errorMessage.getOrAwaitValue(),
            `is`(FakeMoviesRepository.errorMessage)
        )
    }
}
