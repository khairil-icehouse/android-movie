package com.example.movie.util

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.MutableLiveData
import com.example.movie.androidtest.shared.util.getOrAwaitValue
import org.hamcrest.CoreMatchers.`is`
import org.junit.Assert.assertThat
import org.junit.Before
import org.junit.Rule
import org.junit.Test

class SwitchableLiveDataTest {

    private lateinit var sld: SwitchableLiveData<Int>

    @get:Rule
    var instantExecutorRule = InstantTaskExecutorRule()

    @Before
    fun setupTest() {
        sld = SwitchableLiveData()
    }

    @Test
    fun `add a live data to a switchable live data`() {
        // Given new live data
        val mld = MutableLiveData<Int>()
        val expectedValue = 1
        mld.value = expectedValue

        // When the live data is added to empty switchable live data
        sld.switchSource(mld)

        // Then switchable live data return the same value
        val result = sld.getOrAwaitValue()
        assertThat(result, `is`(expectedValue))
    }

    @Test
    fun `replace existing live data`() {
        // Given switchable live data with a live data 1 added
        val value1 = 1
        val mld1 = MutableLiveData<Int>()
        mld1.value = value1
        sld.switchSource(mld1)
        // Given new live data 2
        val value2 = 2
        val mld2 = MutableLiveData<Int>()
        mld2.value = value2

        // When switching live data 1 to live data 2
        sld.switchSource(mld2)

        // Then switchable live data returns the value from live data 2
        val result = sld.getOrAwaitValue()
        assertThat(result, `is`(value2))
    }
}
