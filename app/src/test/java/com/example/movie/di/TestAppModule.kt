package com.example.movie.di

import com.example.movie.test.shared.di.testDomainModule
import com.example.movie.ui.home.HomeViewModel
import com.example.movie.ui.home.NowPlayingMoviesViewModel
import com.example.movie.ui.home.PopularMoviesViewModel
import kotlinx.coroutines.CoroutineDispatcher
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.context.startKoin
import org.koin.core.parameter.parametersOf
import org.koin.dsl.module

val testAppModule = module {
    viewModel { (isSuccess: Boolean, dispatcher: CoroutineDispatcher) ->
        HomeViewModel(
            get { parametersOf(isSuccess, dispatcher) },
            get { parametersOf(isSuccess, dispatcher) }
        )
    }

    viewModel { (isSuccess: Boolean, dispatcher: CoroutineDispatcher) ->
        PopularMoviesViewModel(
            get { parametersOf(isSuccess) },
            get { parametersOf(isSuccess) },
            dispatcher
        )
    }

    viewModel { (isSuccess: Boolean, dispatcher: CoroutineDispatcher) ->
        NowPlayingMoviesViewModel(
            get { parametersOf(isSuccess) },
            get { parametersOf(isSuccess) },
            dispatcher
        )
    }
}

fun setupDependencies() {
    startKoin { modules(listOf(testAppModule, testDomainModule)) }
}
