package com.example.movie.moviedetail.ui

import android.os.Build
import android.os.Bundle
import android.transition.TransitionInflater
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.fragment.navArgs
import coil.api.load
import com.example.movie.moviedetail.databinding.FragmentMovieDetailBinding
import com.example.movie.moviedetail.di.setupDependencies
import com.example.movie.util.setFormattedReleaseDate
import com.example.movie.util.setGenres
import com.example.movie.util.setRatingCount
import com.example.movie.util.setRatingPercentage
import org.koin.androidx.viewmodel.ext.android.viewModel

class MovieDetailFragment : Fragment() {

    private val args: MovieDetailFragmentArgs by navArgs()

    private val movieDetailViewModel: MovieDetailViewModel by viewModel()

    private lateinit var binding: FragmentMovieDetailBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setupDependencies()
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            sharedElementEnterTransition = TransitionInflater.from(context)
                .inflateTransition(android.R.transition.move)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentMovieDetailBinding.inflate(layoutInflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        movieDetailViewModel.setMovieId(args.movieId)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            binding.imageCardView.transitionName = args.transitionName
        }
        subscribeMovie()
    }

    private fun subscribeMovie() {
        movieDetailViewModel.movie.observe(viewLifecycleOwner, Observer {
            it?.let {
                binding.backropImage.load(it.backdropUrl)
                binding.image.load(it.posterUrl)
                binding.toolbar.title = it.title
                binding.overview.text = it.overview
                binding.genres.setGenres(it.genres)
                binding.releaseDate.setFormattedReleaseDate(it.releaseDate)
                binding.rating.setRatingPercentage(it.voteAverage)
                binding.ratingCount.setRatingCount(it.voteCount)
            }
        })
    }
}
