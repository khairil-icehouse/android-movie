package com.example.movie.moviedetail.ui

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import androidx.lifecycle.switchMap
import com.example.movie.domain.usecase.GetMovieUseCase
import kotlinx.coroutines.CoroutineDispatcher

class MovieDetailViewModel(
    private val getMovieUseCase: GetMovieUseCase,
    private val ioDispatcher: CoroutineDispatcher
) : ViewModel() {

    private val movieId = MutableLiveData<Int>()

    val movie = movieId.switchMap { movieId ->
        getMovieUseCase(movieId).asLiveData(ioDispatcher)
    }

    fun setMovieId(id: Int) {
        movieId.value = id
    }
}
