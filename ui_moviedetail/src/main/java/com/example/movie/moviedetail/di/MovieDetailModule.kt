package com.example.movie.moviedetail.di

import com.example.movie.di.IO_DISPATCHER
import com.example.movie.moviedetail.ui.MovieDetailViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.context.loadKoinModules
import org.koin.core.qualifier.named
import org.koin.dsl.module

val movieDetailModule = module {
    viewModel { MovieDetailViewModel(get(), get(named(IO_DISPATCHER))) }
}

private val loadModules by lazy { loadKoinModules(movieDetailModule) }

internal fun setupDependencies() = loadModules
