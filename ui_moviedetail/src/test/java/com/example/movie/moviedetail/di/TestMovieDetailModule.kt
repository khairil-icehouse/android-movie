package com.example.movie.moviedetail.di

import com.example.movie.moviedetail.ui.MovieDetailViewModel
import com.example.movie.test.shared.di.testDomainModule
import kotlinx.coroutines.CoroutineDispatcher
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.context.startKoin
import org.koin.core.parameter.parametersOf
import org.koin.dsl.module

val testMovieDetailModule = module {
    viewModel { (dispatcher: CoroutineDispatcher) ->
        MovieDetailViewModel(
            get { parametersOf(dispatcher) },
            dispatcher
        )
    }
}

fun setupDependencies() {
    startKoin { modules(listOf(testMovieDetailModule, testDomainModule)) }
}
