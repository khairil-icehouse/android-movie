package com.example.movie.moviedetail.ui

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.example.movie.androidtest.shared.util.getOrAwaitValue
import com.example.movie.moviedetail.di.setupDependencies
import com.example.movie.test.shared.util.GenreUtil.fillGenresName
import com.example.movie.test.shared.util.MovieUtil.createMovie
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.runBlockingTest
import kotlinx.coroutines.test.setMain
import org.hamcrest.CoreMatchers.`is`
import org.junit.After
import org.junit.Assert.assertThat
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.koin.core.context.stopKoin
import org.koin.core.parameter.parametersOf
import org.koin.test.KoinTest
import org.koin.test.inject

@ExperimentalCoroutinesApi
class MovieDetailViewModelTest : KoinTest {

    private val viewModel: MovieDetailViewModel by inject { parametersOf(dispatcher) }

    private val dispatcher = TestCoroutineDispatcher()

    @get:Rule
    var instantExecutorRule = InstantTaskExecutorRule()

    @Before
    fun setupTest() {
        setupDependencies()
        Dispatchers.setMain(dispatcher)
    }

    @After
    fun tearDownTest() {
        stopKoin()
    }

    @Test
    fun `get movie after set id`() = dispatcher.runBlockingTest {
        // Given a movie id set to the view model
        val movieId = 1

        // When movie id set to the view model
        viewModel.setMovieId(movieId)

        // Then return movie from repository
        assertThat(viewModel.movie.getOrAwaitValue(), `is`(createMovie(movieId).fillGenresName()))

        // Given second movie id
        val movieId2 = 2

        // When movie id is changed
        viewModel.setMovieId(movieId2)

        // Then movie from view model is also changed
        assertThat(viewModel.movie.getOrAwaitValue(), `is`(createMovie(movieId2).fillGenresName()))
    }
}
