package com.example.movie.data.db

import androidx.room.Room
import androidx.test.platform.app.InstrumentationRegistry
import org.junit.After

abstract class DbTest {

    private lateinit var _db: AppDatabase
    val db: AppDatabase
        get() = _db

    fun setupDb() {
        _db = Room.inMemoryDatabaseBuilder(
            InstrumentationRegistry.getInstrumentation().context,
            AppDatabase::class.java
        ).allowMainThreadQueries().build()
    }

    @After
    fun closeDb() {
        _db.close()
    }
}
