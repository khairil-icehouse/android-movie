package com.example.movie.data.util

import com.example.movie.data.network.GenreNetworkEntity
import com.example.movie.data.network.GenresNetworkEntity
import com.example.movie.test.shared.util.GenreUtil.fakeGenresMapper

object GenreNetworkUtil {

    private val fakeGenreListNetwork = fakeGenresMapper.map {
        GenreNetworkEntity(it.key, it.value)
    }

    val fakeGenresNetwork = GenresNetworkEntity(fakeGenreListNetwork)
}
