package com.example.movie.data.db

import android.os.Build
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.example.movie.data.util.MovieDatabaseEntityUtil.createMovie
import com.example.movie.data.util.MovieDatabaseEntityUtil.createMovieList
import com.example.movie.data.util.MovieListDatabaseEntityUtil.createMovieList
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.test.runBlockingTest
import org.hamcrest.CoreMatchers.`is`
import org.junit.Assert.assertNull
import org.junit.Assert.assertThat
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.annotation.Config

@ExperimentalCoroutinesApi
@Config(sdk = [Build.VERSION_CODES.P])
@RunWith(AndroidJUnit4::class)
class MovieDaoTest : DbTest() {

    private lateinit var dao: MovieDao

    @get:Rule
    var instantTaskExecutor = InstantTaskExecutorRule()

    @Before
    fun setupTest() {
        setupDb()
        dao = db.movieDao()
    }

    @Test
    fun `inserted movies can be selected`() = runBlockingTest {
        val ids = listOf(1, 2, 3, 4)
        val expectedInsertedMovieList = createMovieList(ids)
        dao.insertMovies(expectedInsertedMovieList)
        val movies = dao.getMoviesById(ids).first()
        assertThat(movies, `is`(expectedInsertedMovieList))
    }

    @Test
    fun `database is empty after inserting empty movies`() = runBlockingTest {
        val emptyIds = emptyList<Int>()
        val emptyMovieList = createMovieList(emptyIds)
        dao.insertMovies(emptyMovieList)
        val movies = dao.getMoviesById(listOf(1, 2, 3, 4, 5, 6, 7)).first()
        assertThat(movies, `is`(emptyMovieList))
    }

    @Test
    fun `new movies does not replace old movies in database that have same ids`() =
        runBlockingTest {
            val movie1 = createMovie(1, "one")
            val movie2 = createMovie(1, "two")
            dao.insertMovies(listOf(movie1))
            dao.insertMovies(listOf(movie2))
            val movie = dao.getMovie(1).first()
            assertThat(movie, `is`(movie1))
        }

    @Test
    fun `get a movie by its id`() = runBlockingTest {
        val ids = listOf(1, 2, 3, 4)
        val movieList = createMovieList(ids)
        dao.insertMovies(movieList)
        for (id in ids) {
            val movie = dao.getMovie(id).first()
            assertThat(movie.id, `is`(id))
        }
    }

    @Test
    fun `get a movie but the id is not found returns null`() = runBlockingTest {
        val movie = dao.getMovie(1).first()
        assertNull(movie)
    }

    @Test
    fun `get movies by its ids`() = runBlockingTest {
        val ids = listOf(1, 2, 3, 4)
        val expectedInsertedMovieList = createMovieList(ids)
        dao.insertMovies(expectedInsertedMovieList)
        val movies = dao.getMoviesById(ids).first()
        assertThat(movies, `is`(expectedInsertedMovieList))
    }

    @Test
    fun `get movies by its ids but all of the ids are not found returns empty list`() =
        runBlockingTest {
            val ids = listOf(1, 2, 3, 4)
            val movies = dao.getMoviesById(ids).first()
            assertThat(movies, `is`(emptyList()))
        }

    @Test
    fun `get movies by its ids but some of the ids are not found`() = runBlockingTest {
        val ids = listOf(1, 2, 3, 4)
        val searchIds = listOf(2, 4, 6, 9)
        val intersectedIds = ids.intersect(searchIds).toList()
        val insertedMovieList = createMovieList(ids)
        dao.insertMovies(insertedMovieList)
        val movies = dao.getMoviesById(searchIds).first()
        val expectedMovieList = createMovieList(intersectedIds)
        assertThat(movies, `is`(expectedMovieList))
    }

    @Test
    fun `get movies ordered by its ids`() = runBlockingTest {
        val insertIds = listOf(1, 2, 3, 4)
        val getIdsList = listOf(
            listOf(3, 2, 1, 4),
            listOf(4, 2, 1, 3),
            listOf(4, 3, 2, 1),
            listOf(1, 2, 3, 4)
        )
        val insertedMovieList = createMovieList(insertIds)
        dao.insertMovies(insertedMovieList)
        for (getIds in getIdsList) {
            val movies = dao.getMoviesByIdOrdered(getIds).first()
            val expectedMovieList = createMovieList(getIds)
            assertThat(movies, `is`(expectedMovieList))
        }
    }

    @Test
    fun `get movies ordered by its ids but all of the ids are not found returns empty list`() =
        runBlockingTest {
            val ids = listOf(1, 2, 3, 4)
            val movies = dao.getMoviesByIdOrdered(ids).first()
            assertThat(movies, `is`(emptyList()))
        }

    @Test
    fun `get movies ordered by its ids but some of the ids are not found`() = runBlockingTest {
        val ids = listOf(1, 2, 3, 4).sorted()
        val searchIds = listOf(2, 4, 6, 9).sorted()
        val intersectedIds = ids.intersect(searchIds).toList().sorted()
        val insertedMovieList = createMovieList(ids)
        dao.insertMovies(insertedMovieList)
        val movies = dao.getMoviesByIdOrdered(searchIds).first()
        val expectedMovieList = createMovieList(intersectedIds)
        assertThat(movies, `is`(expectedMovieList))
    }

    @Test
    fun `get movies ordered with repeated ids`() = runBlockingTest {
        val ids = listOf(1, 2, 3, 4)
        val getIds = listOf(1, 2, 1, 4, 3, 4, 1)
        val expectedIds = listOf(2, 3, 4, 1)
        val insertedMovieList = createMovieList(ids)
        dao.insertMovies(insertedMovieList)
        val movies = dao.getMoviesByIdOrdered(getIds).first()
        val expectedMovieList = createMovieList(expectedIds)
        assertThat(movies, `is`(expectedMovieList))
    }

    @Test
    fun `replace existing movies fetch result with same type`() = runBlockingTest {
        val ids1 = listOf(1, 2, 3, 4)
        val ids2 = listOf(1, 2)
        val type = MovieListDatabaseEntity.Type.NOW_PLAYING
        val movieFetchResult1 = createMovieList(type, ids1)
        dao.insertMoviesFetchResult(movieFetchResult1)
        val moviesFetchResult2 = createMovieList(type, ids2)
        dao.insertMoviesFetchResult(moviesFetchResult2)
        val movieList = dao.getMoviesFetchResult(type).first()
        assertThat(movieList, `is`(moviesFetchResult2))
    }
}
