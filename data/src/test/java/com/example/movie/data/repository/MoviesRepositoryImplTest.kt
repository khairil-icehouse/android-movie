package com.example.movie.data.repository

import android.os.Build
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.example.movie.data.db.DbTest
import com.example.movie.data.db.MovieDao
import com.example.movie.data.db.MovieListDatabaseEntity
import com.example.movie.data.network.TmdbService
import com.example.movie.data.util.MovieDatabaseEntityUtil
import com.example.movie.data.util.MovieListDatabaseEntityUtil
import com.example.movie.data.util.MovieNetworkUtil
import com.example.movie.domain.model.Movie
import com.example.movie.domain.model.Resource
import com.example.movie.domain.repository.MoviesRepository
import com.example.movie.test.shared.util.MovieUtil.createMovieList
import com.example.movie.test.shared.util.assertItems
import io.mockk.Called
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.mockk
import io.mockk.slot
import java.net.UnknownHostException
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.annotation.Config
import retrofit2.Response

@ExperimentalCoroutinesApi
@Config(sdk = [Build.VERSION_CODES.P])
@RunWith(AndroidJUnit4::class)
class MoviesRepositoryImplTest : DbTest() {

    private lateinit var repository: MoviesRepository

    private lateinit var dao: MovieDao

    private val service = mockk<TmdbService>()

    @get:Rule
    var instantTaskExecutor = InstantTaskExecutorRule()

    @Before
    fun setupTest() {
        setupDb()
        dao = db.movieDao()
        repository = MoviesRepositoryImpl(dao, service)
    }

    @Test
    fun `get popular movies from existing data in database`() = runBlockingTest {
        // Given existing popular movies in database
        val type = MovieListDatabaseEntity.Type.POPULAR
        val ids = listOf(1, 2)
        val expectedMovieList = createMovieList(ids)
        populateDb(type, expectedMovieList)

        // When getting popular movies from repository
        repository.getPopularMovies(false).assertItems(
            // Then returns data from database immediately without fetching network
            Resource.loading(),
            Resource.success(expectedMovieList)
        )
        coVerify { service.getPopularMovies() wasNot Called }
    }

    @Test
    fun `get popular movies from network`() = runBlockingTest {
        // Given empty database
        // Given popular movies network response
        val ids = listOf(1, 2)
        mockPopularMoviesService(ids)

        // When getting popular movies from repository
        val expectedMovieList = createMovieList(ids)
        repository.getPopularMovies(false).assertItems(
            // Then fetch and return popular movies data from network
            Resource.loading(),
            Resource.loading(emptyList()),
            Resource.success(expectedMovieList)
        )
        coVerify { service.getPopularMovies() }
    }

    @Test
    fun `get popular movies from network failed returns existing data in database`() =
        runBlockingTest {
            // Given existing popular movies data in database
            val type = MovieListDatabaseEntity.Type.POPULAR
            val ids = listOf(1, 2)
            val expectedMovieList = createMovieList(ids)
            populateDb(type, expectedMovieList)
            // Given exception during fetching popular movies from network
            coEvery { service.getPopularMovies() } throws UnknownHostException()

            // When getting popular movies from repository
            repository.getPopularMovies(true).assertItems(
                // Then return existing data from database with error message
                Resource.loading(),
                Resource.loading(expectedMovieList),
                Resource.error("Unknown error", expectedMovieList)
            )
        }

    @Test
    fun `refresh popular movies in database`() = runBlockingTest {
        // Given existing popular movies data in database
        val type = MovieListDatabaseEntity.Type.POPULAR
        val ids1 = listOf(1, 2)
        val expectedMovieList1 = createMovieList(ids1)
        populateDb(type, expectedMovieList1)
        // Given new popular movies response from network
        val ids2 = listOf(3, 4)
        val expectedMovieList2 = createMovieList(ids2)
        mockPopularMoviesService(ids2)

        // When refreshing popular movies from repository
        repository.getPopularMovies(true).assertItems(
            Resource.loading(),
            // Then return existing popular movies from database while fetching new data
            Resource.loading(expectedMovieList1),
            // Then return new popular movies from network
            Resource.success(expectedMovieList2)
        )
    }

    @Test
    fun `fetch popular movies next page`() = runBlockingTest {
        // Given existing popular movies data in database
        val type = MovieListDatabaseEntity.Type.POPULAR
        val ids1 = listOf(1, 2)
        val existingMovieList = createMovieList(ids1)
        populateDb(type, existingMovieList)
        // Given popular movies page 2 from network
        val ids2 = listOf(3, 4)
        mockPopularMoviesService(ids2)

        // When getting popular movies next page
        repository.getPopularMoviesNextPage().assertItems(
            // Then return loading and success
            Resource.loading(),
            Resource.success()
        )
        coVerify { service.getPopularMovies(page = 2) }

        // When getting popular movies from repository
        val expectedMovieList = createMovieList(ids1 + ids2)
        repository.getPopularMovies(false).assertItems(
            // Then return popular movies from page 1 and 2 combined
            Resource.loading(),
            Resource.success(expectedMovieList)
        )
    }

    @Test
    fun `get now playing movies from existing data in database`() = runBlockingTest {
        // Given existing now playing movies in database
        val type = MovieListDatabaseEntity.Type.NOW_PLAYING
        val ids = listOf(1, 2)
        val expectedMovieList = createMovieList(ids)
        populateDb(type, expectedMovieList)

        // When getting now playing movies from repository
        repository.getNowPlayingMovies(false).assertItems(
            // Then returns data from database immediately without fetching network
            Resource.loading(),
            Resource.success(expectedMovieList)
        )
        coVerify { service.getNowPlayingMovies() wasNot Called }
    }

    @Test
    fun `get now playing movies from network`() = runBlockingTest {
        // Given empty database
        // Given now playing movies network response
        val ids = listOf(1, 2)
        mockNowPlayingMoviesService(ids)

        // When getting now playing movies from repository
        val expectedMovieList = createMovieList(ids)
        repository.getNowPlayingMovies(false).assertItems(
            // Then fetch and return now playing movies data from network
            Resource.loading(),
            Resource.loading(emptyList()),
            Resource.success(expectedMovieList)
        )
        coVerify { service.getNowPlayingMovies() }
    }

    @Test
    fun `get now playing movies from network failed returns existing data in database`() =
        runBlockingTest {
            // Given existing now playing movies data in database
            val type = MovieListDatabaseEntity.Type.NOW_PLAYING
            val ids = listOf(1, 2)
            val expectedMovieList = createMovieList(ids)
            populateDb(type, expectedMovieList)
            // Given exception during fetching now playing movies from network
            coEvery { service.getNowPlayingMovies() } throws UnknownHostException()

            // When getting now playing movies from repository
            repository.getNowPlayingMovies(true).assertItems(
                // Then return existing data from database with error message
                Resource.loading(),
                Resource.loading(expectedMovieList),
                Resource.error("Unknown error", expectedMovieList)
            )
        }

    @Test
    fun `refresh now playing movies in database`() = runBlockingTest {
        // Given existing now playing movies data in database
        val type = MovieListDatabaseEntity.Type.NOW_PLAYING
        val ids1 = listOf(1, 2)
        val expectedMovieList1 = createMovieList(ids1)
        populateDb(type, expectedMovieList1)
        // Given new now playing movies response from network
        val ids2 = listOf(3, 4)
        val expectedMovieList2 = createMovieList(ids2)
        mockNowPlayingMoviesService(ids2)

        // When refreshing now playing movies from repository
        repository.getNowPlayingMovies(true).assertItems(
            Resource.loading(),
            // Then return existing now playing movies from database while fetching new data
            Resource.loading(expectedMovieList1),
            // Then return new now playing movies from network
            Resource.success(expectedMovieList2)
        )
    }

    @Test
    fun `fetch now playing movies next page`() = runBlockingTest {
        // Given existing now playing movies data in database
        val type = MovieListDatabaseEntity.Type.NOW_PLAYING
        val ids1 = listOf(1, 2)
        val existingMovieList = createMovieList(ids1)
        populateDb(type, existingMovieList)
        // Given now playing movies page 2 from network
        val ids2 = listOf(3, 4)
        mockNowPlayingMoviesService(ids2)

        // When getting now playing movies next page
        repository.getNowPlayingMoviesNextPage().assertItems(
            // Then return loading and success
            Resource.loading(),
            Resource.success()
        )
        coVerify { service.getNowPlayingMovies(page = 2) }

        // When getting now playing movies from repository
        val expectedMovieList = createMovieList(ids1 + ids2)
        repository.getNowPlayingMovies(false).assertItems(
            // Then return now playing movies from page 1 and 2 combined
            Resource.loading(),
            Resource.success(expectedMovieList)
        )
    }

    private fun mockPopularMoviesService(ids: List<Int>) {
        val slot = slot<Int>()
        coEvery { service.getPopularMovies(capture(slot)) } answers {
            Response.success(
                MovieNetworkUtil.createMovieList(ids, slot.captured)
            )
        }
    }

    private fun mockNowPlayingMoviesService(ids: List<Int>) {
        val slot = slot<Int>()
        coEvery { service.getNowPlayingMovies(capture(slot)) } answers {
            Response.success(
                MovieNetworkUtil.createMovieList(ids, slot.captured)
            )
        }
    }

    private fun populateDb(type: MovieListDatabaseEntity.Type, movies: List<Movie>) {
        val ids = movies.map { it.id }
        val moviesFetchResult = MovieListDatabaseEntityUtil.createMovieList(type, ids)
        val movieList = MovieDatabaseEntityUtil.createMovieList(ids)
        dao.insertMoviesFetchResult(moviesFetchResult)
        dao.insertMovies(movieList)
    }
}
