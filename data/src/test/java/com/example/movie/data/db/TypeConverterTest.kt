package com.example.movie.data.db

import kotlin.random.Random
import org.hamcrest.CoreMatchers.`is`
import org.junit.Assert.assertThat
import org.junit.Test

class TypeConverterTest {

    @Test
    fun `convert list of int to string`() {
        repeat(100) {
            val randomList = MutableList(100) { Random.nextInt(50) }.toList()
            val string = TypeConverter.intListToString(randomList)
            val list = TypeConverter.stringToIntList(string)
            assertThat(list, `is`(randomList))
        }
    }

    @Test
    fun `convert movie list type to int`() {
        val types = MovieListDatabaseEntity.Type.values()
        for (type in types) {
            val int = TypeConverter.movieListTypeToInt(type)
            val convertedType = TypeConverter.intToMovieListType(int)
            assertThat(convertedType, `is`(type))
        }
    }
}
