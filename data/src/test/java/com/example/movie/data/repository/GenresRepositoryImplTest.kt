package com.example.movie.data.repository

import com.example.movie.data.cache.GenresCacheDataSource
import com.example.movie.data.network.TmdbService
import com.example.movie.data.util.GenreNetworkUtil.fakeGenresNetwork
import com.example.movie.domain.model.Resource
import com.example.movie.domain.repository.GenresRepository
import com.example.movie.test.shared.util.GenreUtil.fakeGenresMapper
import com.example.movie.test.shared.util.assertCompleteStream
import io.mockk.coEvery
import io.mockk.mockk
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Test
import retrofit2.Response

@ExperimentalCoroutinesApi
class GenresRepositoryImplTest {

    private lateinit var repository: GenresRepository

    private val service = mockk<TmdbService>()

    @Test
    fun `get genres mapper from network`() = runBlockingTest {
        // Given repository with empty cache
        repository = GenresRepositoryImpl(GenresCacheDataSource(), service)
        // Given network response
        coEvery { service.getGenres() } returns Response.success(fakeGenresNetwork)

        // When getting genres mapper from repository
        val result = repository.getGenresMapper()

        // Then repository check cache, load from network, return result from network
        result.assertCompleteStream(
            Resource.loading(),
            Resource.loading(),
            Resource.success(fakeGenresMapper)
        )
    }

    @Test
    fun `get genres mapper from cache`() = runBlockingTest {
        // Given repository with cache
        val cache = GenresCacheDataSource()
        cache.genresMapper = fakeGenresMapper
        repository = GenresRepositoryImpl(cache, service)

        // When getting genres mapper from repository
        val result = repository.getGenresMapper()

        // Then repository check cache, return result from cache without fetching from network
        result.assertCompleteStream(
            Resource.loading(),
            Resource.success(fakeGenresMapper)
        )
    }
}
