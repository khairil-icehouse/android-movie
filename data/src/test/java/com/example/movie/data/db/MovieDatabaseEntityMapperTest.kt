package com.example.movie.data.db

import com.example.movie.domain.model.Genre
import com.example.movie.domain.model.Movie
import com.example.movie.test.shared.util.GenreUtil.fakeGenreIds
import org.hamcrest.CoreMatchers.`is`
import org.junit.Assert.assertThat
import org.junit.Test

class MovieDatabaseEntityMapperTest {

    @Test
    fun `map movie database entity into domain movie`() {
        val id = 12
        val title = "test"
        val posterUrl = "http://google.com"
        val overview = "lorem ipsum dolor sit amet"
        val genreIds = fakeGenreIds
        val movieDatabaseEntity = MovieDatabaseEntity(
            id = id,
            title = title,
            posterUrl = posterUrl,
            overview = overview,
            genreIds = genreIds,
            voteCount = 0,
            voteAverage = 0.0,
            releaseDate = "",
            backdropUrl = ""
        )
        val expectedMovie = Movie(
            id = id,
            title = title,
            posterUrl = posterUrl,
            overview = overview,
            genres = genreIds.map { Genre(it, "") },
            voteCount = 0,
            voteAverage = 0.0,
            releaseDate = "",
            backdropUrl = ""
        )
        assertThat(movieDatabaseEntity.toDomain(), `is`(expectedMovie))
    }
}
