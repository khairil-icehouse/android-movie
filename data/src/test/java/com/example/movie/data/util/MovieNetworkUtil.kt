package com.example.movie.data.util

import com.example.movie.data.network.MovieListNetworkEntity
import com.example.movie.data.network.MovieNetworkEntity
import com.example.movie.test.shared.util.GenreUtil.fakeGenreIds

object MovieNetworkUtil {

    private fun createMovie(id: Int, title: String? = "title$id") = MovieNetworkEntity(
        id = id,
        title = title,
        posterPath = null,
        overview = null,
        genreIds = fakeGenreIds,
        voteCount = null,
        voteAverage = null,
        releaseDate = null,
        backdropPath = null
    )

    fun createMovieList(ids: List<Int>, page: Int = 1) = MovieListNetworkEntity(
        page = page,
        totalResults = 1,
        totalPages = 10,
        results = MutableList(ids.size) { createMovie(ids[it]) }.toList()
    )
}
