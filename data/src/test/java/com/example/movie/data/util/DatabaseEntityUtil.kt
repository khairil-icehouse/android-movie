package com.example.movie.data.util

import com.example.movie.data.db.MovieDatabaseEntity
import com.example.movie.data.db.MovieListDatabaseEntity
import com.example.movie.data.network.toDatabase
import com.example.movie.test.shared.util.GenreUtil.fakeGenreIds

object MovieDatabaseEntityUtil {

    fun createMovie(id: Int, title: String = "title$id") =
        MovieDatabaseEntity(
            id = id,
            title = title,
            posterUrl = "",
            overview = "",
            genreIds = fakeGenreIds,
            voteCount = 0,
            voteAverage = 0.0,
            releaseDate = "",
            backdropUrl = ""
        )

    fun createMovieList(ids: List<Int>): List<MovieDatabaseEntity> = MutableList(ids.size) {
        createMovie(ids[it])
    }
}

object MovieListDatabaseEntityUtil {

    fun createMovieList(
        type: MovieListDatabaseEntity.Type,
        ids: List<Int>,
        page: Int = 1
    ): MovieListDatabaseEntity {
        return MovieNetworkUtil.createMovieList(ids, page).toDatabase(type)
    }
}
