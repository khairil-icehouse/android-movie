package com.example.movie.data.repository

import com.example.movie.domain.model.Resource
import com.example.movie.test.shared.util.assertCompleteStream
import java.net.UnknownHostException
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.test.runBlockingTest
import okhttp3.MediaType
import okhttp3.Protocol
import okhttp3.Request
import okhttp3.ResponseBody
import org.junit.Before
import org.junit.Test
import retrofit2.Response

@ExperimentalCoroutinesApi
class NetworkBoundResourceTest {

    private lateinit var networkBoundResource: NetworkBoundResource<Foo, Foo>

    private lateinit var handleSaveNetworkResult: (Foo) -> Unit

    private lateinit var handleShouldFetch: (Foo?) -> Boolean

    private lateinit var handleFetchFromNetwork: () -> Response<Foo>

    private val dbFlow: Flow<Foo?>
        get() = flow {
            emit(dbValue)
        }

    private var dbValue: Foo? = null

    private val errorResponse = Response.error<Foo>(
        ResponseBody.create(MediaType.parse("text/html"), "error"),
        okhttp3.Response.Builder()
            .code(500)
            .request(Request.Builder().url("http://localhost/").build())
            .message("error")
            .protocol(Protocol.HTTP_1_1)
            .build()
    )

    @Before
    fun setupTest() {
        dbValue = null
        networkBoundResource = object : NetworkBoundResource<Foo, Foo>() {
            override suspend fun saveNetworkResult(item: Foo) {
                handleSaveNetworkResult(item)
            }

            override fun shouldFetch(data: Foo?): Boolean {
                return handleShouldFetch(data)
            }

            override fun loadFromDb(): Flow<Foo?> {
                return dbFlow
            }

            override suspend fun fetchFromNetwork(): Response<Foo> {
                return handleFetchFromNetwork()
            }
        }
    }

    @Test
    fun `success from network`() = runBlockingTest {
        val foo1 = Foo(1)
        handleShouldFetch = { it == null }
        handleFetchFromNetwork = { Response.success(foo1) }
        handleSaveNetworkResult = { dbValue = it }
        networkBoundResource.asFlow().assertCompleteStream(
            Resource.loading(),
            Resource.loading(),
            Resource.success(foo1)
        )
    }

    @Test
    fun `failure from network code`() = runBlockingTest {
        handleShouldFetch = { it == null }
        handleFetchFromNetwork = { errorResponse }
        handleSaveNetworkResult = { dbValue = it }
        networkBoundResource.asFlow().assertCompleteStream(
            Resource.loading(),
            Resource.loading(),
            Resource.error("error")
        )
    }

    @Test
    fun `failure from network exception`() = runBlockingTest {
        handleShouldFetch = { it == null }
        handleFetchFromNetwork = { throw UnknownHostException("error") }
        handleSaveNetworkResult = { dbValue = it }
        networkBoundResource.asFlow().assertCompleteStream(
            Resource.loading(),
            Resource.loading(),
            Resource.error("error")
        )
    }

    @Test
    fun `success from database without network`() = runBlockingTest {
        val foo1 = Foo(1)
        dbValue = foo1
        handleShouldFetch = { it == null }
        networkBoundResource.asFlow().assertCompleteStream(
            Resource.loading(),
            Resource.success(foo1)
        )
    }

    @Test
    fun `success from database with network failure`() = runBlockingTest {
        val foo1 = Foo(1)
        dbValue = foo1
        handleShouldFetch = { true }
        handleFetchFromNetwork = { throw UnknownHostException("error") }
        networkBoundResource.asFlow().assertCompleteStream(
            Resource.loading(),
            Resource.loading(foo1),
            Resource.error("error", foo1)
        )
    }

    @Test
    fun `update existing data in database from network`() = runBlockingTest {
        val foo1 = Foo(1)
        val foo2 = Foo(2)
        dbValue = foo1
        handleShouldFetch = { true }
        handleFetchFromNetwork = { Response.success(foo2) }
        handleSaveNetworkResult = { dbValue = it }
        networkBoundResource.asFlow().assertCompleteStream(
            Resource.loading(),
            Resource.loading(foo1),
            Resource.success(foo2)
        )
    }

    @Test
    fun `call asFlow for the 2nd time only emits last emitted success value`() = runBlockingTest {
        val foo1 = Foo(1)
        dbValue = foo1
        handleShouldFetch = { false }
        networkBoundResource.asFlow().assertCompleteStream(
            Resource.loading(),
            Resource.success(foo1)
        )
        networkBoundResource.asFlow().assertCompleteStream(Resource.success(foo1))
    }

    @Test
    fun `call asFlow for the 2nd time only emits last emitted error value`() = runBlockingTest {
        val foo1 = Foo(1)
        dbValue = foo1
        handleShouldFetch = { true }
        handleFetchFromNetwork = { errorResponse }
        networkBoundResource.asFlow().assertCompleteStream(
            Resource.loading(),
            Resource.loading(foo1),
            Resource.error("error", foo1)
        )
        networkBoundResource.asFlow().assertCompleteStream(Resource.error("error", foo1))
    }

    private data class Foo(val value: Int)
}
