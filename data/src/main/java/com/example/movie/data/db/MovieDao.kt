package com.example.movie.data.db

import android.util.SparseIntArray
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map

@Dao
abstract class MovieDao {

    @Query("SELECT * FROM MovieDatabaseEntity WHERE id = :movieId LIMIT 1")
    abstract fun getMovie(movieId: Int): Flow<MovieDatabaseEntity>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    abstract fun insertMovies(movies: List<MovieDatabaseEntity>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insertMoviesFetchResult(result: MovieListDatabaseEntity)

    @Query("SELECT * FROM MovieListDatabaseEntity WHERE type = :movieListType LIMIT 1")
    abstract fun getMoviesFetchResult(
        movieListType: MovieListDatabaseEntity.Type
    ): Flow<MovieListDatabaseEntity?>

    @Query("SELECT * FROM MovieDatabaseEntity where id in (:ids)")
    abstract fun getMoviesById(ids: List<Int>): Flow<List<MovieDatabaseEntity>>

    fun getMoviesByIdOrdered(ids: List<Int>): Flow<List<MovieDatabaseEntity>> {
        val order = SparseIntArray()
        ids.forEachIndexed { idx, id ->
            order.put(id, idx)
        }
        return getMoviesById(ids).map {
            it.sortedWith(compareBy { movie ->
                order.get(movie.id)
            })
        }
    }
}
