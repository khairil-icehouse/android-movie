package com.example.movie.data.repository

import com.example.movie.data.db.MovieDao
import com.example.movie.data.db.MovieListDatabaseEntity
import com.example.movie.data.db.toDomain
import com.example.movie.data.network.MovieListNetworkEntity
import com.example.movie.data.network.TmdbService
import com.example.movie.domain.model.Movie
import com.example.movie.domain.model.Resource
import com.example.movie.domain.repository.MoviesRepository
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import retrofit2.Response

class MoviesRepositoryImpl(
    private val movieDao: MovieDao,
    private val tmdbService: TmdbService
) : MoviesRepository {

    @ExperimentalCoroutinesApi
    override fun getPopularMovies(forceRefresh: Boolean): Flow<Resource<List<Movie>>> {
        return object : GetMoviesNetworkBoundResource(
            movieDao,
            MovieListDatabaseEntity.Type.POPULAR,
            forceRefresh
        ) {
            override suspend fun fetchFromNetwork(): Response<MovieListNetworkEntity> {
                return tmdbService.getPopularMovies()
            }
        }.asFlow()
    }

    @ExperimentalCoroutinesApi
    override fun getNowPlayingMovies(forceRefresh: Boolean): Flow<Resource<List<Movie>>> {
        return object : GetMoviesNetworkBoundResource(
            movieDao,
            MovieListDatabaseEntity.Type.NOW_PLAYING,
            forceRefresh
        ) {
            override suspend fun fetchFromNetwork(): Response<MovieListNetworkEntity> {
                return tmdbService.getNowPlayingMovies()
            }
        }.asFlow()
    }

    override fun getPopularMoviesNextPage(): Flow<Resource<Unit>> {
        return object : FetchMoviesNextPageTask(movieDao, MovieListDatabaseEntity.Type.POPULAR) {
            override suspend fun fetchFromNetwork(page: Int): Response<MovieListNetworkEntity> {
                return tmdbService.getPopularMovies(page)
            }
        }.asFlow()
    }

    override fun getNowPlayingMoviesNextPage(): Flow<Resource<Unit>> {
        return object :
            FetchMoviesNextPageTask(movieDao, MovieListDatabaseEntity.Type.NOW_PLAYING) {
            override suspend fun fetchFromNetwork(page: Int): Response<MovieListNetworkEntity> {
                return tmdbService.getNowPlayingMovies(page)
            }
        }.asFlow()
    }

    override fun getMovie(movieId: Int): Flow<Movie> {
        return movieDao.getMovie(movieId).map {
            it.toDomain()
        }
    }
}
