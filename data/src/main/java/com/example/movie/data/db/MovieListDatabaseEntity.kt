package com.example.movie.data.db

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class MovieListDatabaseEntity(
    @PrimaryKey
    val type: Type,
    val page: Int,
    val totalResults: Int,
    val totalPages: Int,
    val movieIds: List<Int>
) {

    val nextPage: Int?
        get() = if (page + 1 <= totalPages) {
            page + 1
        } else {
            null
        }

    enum class Type {
        POPULAR, NOW_PLAYING;

        companion object {
            fun fromInt(int: Int): Type = values().first { it.ordinal == int }
        }
    }
}
