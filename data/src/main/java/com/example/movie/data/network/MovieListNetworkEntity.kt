package com.example.movie.data.network

import com.example.movie.data.db.MovieListDatabaseEntity
import com.google.gson.annotations.SerializedName

data class MovieListNetworkEntity(
    @SerializedName("page")
    val page: Int?,
    @SerializedName("total_results")
    val totalResults: Int?,
    @SerializedName("total_pages")
    val totalPages: Int?,
    @SerializedName("results")
    val results: List<MovieNetworkEntity>?
)

fun MovieListNetworkEntity.toDatabase(
    type: MovieListDatabaseEntity.Type,
    previousResult: MovieListDatabaseEntity? = null
): MovieListDatabaseEntity {
    val movieIds = ArrayList<Int>()
    if (previousResult != null) {
        movieIds.addAll(previousResult.movieIds)
    }
    movieIds.addAll(results?.map { it.id ?: 0 } ?: emptyList())
    return MovieListDatabaseEntity(
        type = type,
        page = page ?: 0,
        totalResults = totalResults ?: 0,
        totalPages = totalPages ?: 0,
        movieIds = movieIds
    )
}
