package com.example.movie.data.repository

import com.example.movie.data.db.MovieDao
import com.example.movie.data.db.MovieListDatabaseEntity
import com.example.movie.data.network.MovieListNetworkEntity
import com.example.movie.data.network.toDatabase
import kotlinx.coroutines.flow.Flow

abstract class FetchMoviesNextPageTask(
    private val movieDao: MovieDao,
    private val movieListType: MovieListDatabaseEntity.Type
) :
    FetchNextPageTask<MovieListDatabaseEntity, MovieListNetworkEntity>() {

    override fun loadFromDb(): Flow<MovieListDatabaseEntity?> {
        return movieDao.getMoviesFetchResult(movieListType)
    }

    override suspend fun getNextPage(result: MovieListDatabaseEntity?): Int? {
        return if (result?.nextPage == null) {
            null
        } else {
            result.nextPage
        }
    }

    override suspend fun saveNetworkResult(
        currentResult: MovieListDatabaseEntity,
        item: MovieListNetworkEntity
    ) {
        val results = item.results ?: emptyList()
        movieDao.insertMovies(results.toDatabase())
        movieDao.insertMoviesFetchResult(item.toDatabase(movieListType, currentResult))
    }
}
