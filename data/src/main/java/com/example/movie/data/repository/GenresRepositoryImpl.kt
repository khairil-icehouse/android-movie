package com.example.movie.data.repository

import com.example.movie.data.cache.GenresCacheDataSource
import com.example.movie.data.network.GenresNetworkEntity
import com.example.movie.data.network.TmdbService
import com.example.movie.domain.model.Resource
import com.example.movie.domain.repository.GenresRepository
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import retrofit2.Response

class GenresRepositoryImpl(
    private val genresCacheDataSource: GenresCacheDataSource,
    private val tmdbService: TmdbService
) : GenresRepository {

    @ExperimentalCoroutinesApi
    override fun getGenresMapper(): Flow<Resource<Map<Int, String>>> {
        return object : NetworkBoundResource<Map<Int, String>, GenresNetworkEntity>() {
            override suspend fun saveNetworkResult(item: GenresNetworkEntity) {
                val genres = item.genres
                val mapper = HashMap<Int, String>()
                genres.forEach {
                    if (it?.id != null) {
                        mapper[it.id] = it.name ?: ""
                    }
                }
                genresCacheDataSource.genresMapper = mapper
            }

            override fun shouldFetch(data: Map<Int, String>?): Boolean {
                return data == null
            }

            override fun loadFromDb(): Flow<Map<Int, String>?> {
                return flow { emit(genresCacheDataSource.genresMapper) }
            }

            override suspend fun fetchFromNetwork(): Response<GenresNetworkEntity> {
                return tmdbService.getGenres()
            }
        }.asFlow()
    }
}
