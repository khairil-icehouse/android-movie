package com.example.movie.data.di

import com.example.movie.data.cache.GenresCacheDataSource
import com.example.movie.data.db.AppDatabase
import com.example.movie.data.network.BASE_URL
import com.example.movie.data.network.TmdbService
import com.example.movie.data.repository.GenresRepositoryImpl
import com.example.movie.data.repository.MoviesRepositoryImpl
import com.example.movie.domain.repository.GenresRepository
import com.example.movie.domain.repository.MoviesRepository
import org.koin.android.ext.koin.androidApplication
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

val dataModule = module {
    single<TmdbService> {
        Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
            .create(TmdbService::class.java)
    }

    single<MoviesRepository> {
        MoviesRepositoryImpl(get(), get())
    }

    single {
        AppDatabase.getInstance(androidApplication())
    }

    single {
        get<AppDatabase>().movieDao()
    }

    single<GenresRepository> {
        GenresRepositoryImpl(get(), get())
    }

    single {
        GenresCacheDataSource()
    }
}
