package com.example.movie.data.repository

import com.example.movie.data.db.MovieDao
import com.example.movie.data.db.MovieListDatabaseEntity
import com.example.movie.data.db.toDomain
import com.example.movie.data.network.MovieListNetworkEntity
import com.example.movie.data.network.toDatabase
import com.example.movie.domain.model.Movie
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flatMapLatest
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.map

abstract class GetMoviesNetworkBoundResource(
    private val movieDao: MovieDao,
    private val movieListType: MovieListDatabaseEntity.Type,
    private val forceRefresh: Boolean
) :
    NetworkBoundResource<List<Movie>, MovieListNetworkEntity>() {

    override suspend fun saveNetworkResult(item: MovieListNetworkEntity) {
        val results = item.results ?: emptyList()
        movieDao.insertMoviesFetchResult(item.toDatabase(movieListType))
        movieDao.insertMovies(results.toDatabase())
    }

    override fun shouldFetch(data: List<Movie>?): Boolean {
        return data.isNullOrEmpty() || forceRefresh
    }

    @ExperimentalCoroutinesApi
    override fun loadFromDb(): Flow<List<Movie>?> {
        return movieDao.getMoviesFetchResult(movieListType).flatMapLatest {
            if (it == null) {
                flow { emit(emptyList<Movie>()) }
            } else {
                movieDao.getMoviesByIdOrdered(it.movieIds).map { it.toDomain() }
            }
        }
    }
}
