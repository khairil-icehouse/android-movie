package com.example.movie.data.repository

import androidx.annotation.MainThread
import androidx.annotation.WorkerThread
import com.example.movie.domain.model.Resource
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.FlowCollector
import kotlinx.coroutines.flow.emitAll
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.map
import retrofit2.Response
import timber.log.Timber

abstract class NetworkBoundResource<ResultType, RequestType> {

    private lateinit var lastEmittedResource: Flow<Resource<ResultType>>

    @ExperimentalCoroutinesApi
    fun asFlow() = flow<Resource<ResultType>> {
        if (::lastEmittedResource.isInitialized) {
            emitAll(lastEmittedResource)
            return@flow
        }

        emit(Resource.loading())
        val dbValue = loadFromDb().first()

        if (!shouldFetch(dbValue)) {
            saveAndEmitAll(loadFromDb().map { Resource.success(it) })
            return@flow
        }

        emit(Resource.loading(dbValue))

        val apiResponse: Response<RequestType>
        try {
            Timber.i("Network loading")
            apiResponse = fetchFromNetwork()
        } catch (e: Exception) {
            onFetchFailed(e.message)
            return@flow
        }

        val responseBody = apiResponse.body()
        if (apiResponse.isSuccessful && responseBody != null) {
            onFetchSucceeded(responseBody)
        } else {
            onFetchFailed(apiResponse.message())
        }
    }

    @ExperimentalCoroutinesApi
    private suspend fun FlowCollector<Resource<ResultType>>.onFetchFailed(errorMessage: String?) {
        Timber.i("Network failure")
        saveAndEmitAll(loadFromDb().map { Resource.error(errorMessage ?: "Unknown error", it) })
    }

    @ExperimentalCoroutinesApi
    private suspend fun FlowCollector<Resource<ResultType>>.onFetchSucceeded(
        fetchResult: RequestType
    ) {
        Timber.i("Network success, saving to database")
        saveNetworkResult(fetchResult)
        saveAndEmitAll(loadFromDb().map { Resource.success(it) })
    }

    @ExperimentalCoroutinesApi
    private suspend fun FlowCollector<Resource<ResultType>>.saveAndEmitAll(
        flow: Flow<Resource<ResultType>>
    ) {
        lastEmittedResource = flow
        emitAll(flow)
    }

    @WorkerThread
    protected abstract suspend fun saveNetworkResult(item: RequestType)

    @MainThread
    protected abstract fun shouldFetch(data: ResultType?): Boolean

    @MainThread
    protected abstract fun loadFromDb(): Flow<ResultType?>

    @MainThread
    protected abstract suspend fun fetchFromNetwork(): Response<RequestType>
}
