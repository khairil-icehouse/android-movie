package com.example.movie.data.db

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.example.movie.domain.model.Genre
import com.example.movie.domain.model.Movie

@Entity
data class MovieDatabaseEntity(
    @PrimaryKey
    val id: Int,
    val title: String,
    val posterUrl: String,
    val overview: String,
    val genreIds: List<Int>,
    val voteCount: Int,
    val voteAverage: Double,
    val releaseDate: String,
    val backdropUrl: String
)

fun List<MovieDatabaseEntity>.toDomain(): List<Movie> = map {
    it.toDomain()
}

fun MovieDatabaseEntity.toDomain(): Movie = Movie(
    id = id,
    title = title,
    posterUrl = posterUrl,
    overview = overview,
    genres = genreIds.map { Genre(it, "") },
    voteCount = voteCount,
    voteAverage = voteAverage,
    releaseDate = releaseDate,
    backdropUrl = backdropUrl
)
