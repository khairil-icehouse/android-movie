package com.example.movie.data.network

import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface TmdbService {

    @GET(POPULAR_MOVIES_PATH)
    suspend fun getPopularMovies(
        @Query(PAGE_KEY) page: Int = PAGE_DEF_VALUE,
        @Query(SORT_BY_KEY) sortBy: String = SORT_BY_DEF_VALUE,
        @Query(API_KEY_KEY) apiKey: String = API_KEY_DEF_VALUE
    ): Response<MovieListNetworkEntity>

    @GET(NOW_PLAYING_MOVIES_PATH)
    suspend fun getNowPlayingMovies(
        @Query(PAGE_KEY) page: Int = PAGE_DEF_VALUE,
        @Query(SORT_BY_KEY) sortBy: String = SORT_BY_DEF_VALUE,
        @Query(API_KEY_KEY) apiKey: String = API_KEY_DEF_VALUE
    ): Response<MovieListNetworkEntity>

    @GET(GENRES_PATH)
    suspend fun getGenres(
        @Query(API_KEY_KEY) apiKey: String = API_KEY_DEF_VALUE
    ): Response<GenresNetworkEntity>

    companion object {
        const val POPULAR_MOVIES_PATH = "discover/movie"
        const val NOW_PLAYING_MOVIES_PATH = "movie/now_playing"
        const val GENRES_PATH = "genre/movie/list"

        const val API_KEY_KEY = "api_key"
        const val API_KEY_DEF_VALUE = "4a01a777bc601f84dc9c862d70bea3fb"

        const val SORT_BY_KEY = "sort_by"
        const val SORT_BY_DEF_VALUE = "popularity.desc"

        const val PAGE_KEY = "page"
        const val PAGE_DEF_VALUE = 1
    }
}
