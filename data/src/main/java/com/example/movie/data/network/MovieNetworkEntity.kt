package com.example.movie.data.network

import com.example.movie.data.db.MovieDatabaseEntity
import com.google.gson.annotations.SerializedName

data class MovieNetworkEntity(
    @SerializedName("id")
    val id: Int?,
    @SerializedName("title")
    val title: String?,
    @SerializedName("poster_path")
    val posterPath: String?,
    @SerializedName("overview")
    val overview: String?,
    @SerializedName("genre_ids")
    val genreIds: List<Int>?,
    @SerializedName("vote_count")
    val voteCount: Int?,
    @SerializedName("vote_average")
    val voteAverage: Double?,
    @SerializedName("release_date")
    val releaseDate: String?,
    @SerializedName("backdrop_path")
    val backdropPath: String?
) {

    val posterUrl: String?
        get() = posterPath?.let { IMAGE_BASE_URL + it }

    val backdropUrl: String?
        get() = backdropPath?.let { IMAGE_BASE_URL + it }
}

fun List<MovieNetworkEntity>.toDatabase(): List<MovieDatabaseEntity> = map {
    MovieDatabaseEntity(
        id = it.id ?: 0,
        title = it.title ?: "",
        posterUrl = it.posterUrl ?: "",
        overview = it.overview ?: "",
        genreIds = it.genreIds ?: emptyList(),
        voteCount = it.voteCount ?: 0,
        voteAverage = it.voteAverage ?: 0.0,
        releaseDate = it.releaseDate ?: "",
        backdropUrl = it.backdropUrl ?: ""
    )
}
