package com.example.movie.data.network

import com.google.gson.annotations.SerializedName

data class GenresNetworkEntity(
    @SerializedName("genres")
    val genres: List<GenreNetworkEntity?>
)

data class GenreNetworkEntity(
    @SerializedName("id")
    val id: Int?,
    @SerializedName("name")
    val name: String?
)
