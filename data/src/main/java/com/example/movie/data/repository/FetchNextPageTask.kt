package com.example.movie.data.repository

import com.example.movie.domain.model.Resource
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.FlowCollector
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.flow
import retrofit2.Response
import timber.log.Timber

abstract class FetchNextPageTask<ResultType, RequestType> {

    fun asFlow() = flow<Resource<Unit>> {
        emit(Resource.loading())
        val currentResult = loadFromDb().first()
        val nextPage = getNextPage(currentResult)
        Timber.i("Next Page: $nextPage")
        if (currentResult == null || nextPage == null) {
            emit(Resource.success())
            return@flow
        }

        Timber.i("Network fetching nextpage $nextPage")
        val apiResponse: Response<RequestType>
        try {
            apiResponse = fetchFromNetwork(nextPage)
        } catch (e: Exception) {
            onFetchFailed(e.message)
            return@flow
        }

        val responseBody = apiResponse.body()
        if (apiResponse.isSuccessful && responseBody != null) {
            onFetchSucceeded(currentResult, responseBody)
        } else {
            onFetchFailed(apiResponse.message())
        }
    }

    private suspend fun FlowCollector<Resource<Unit>>.onFetchSucceeded(
        currentResult: ResultType,
        networkResult: RequestType
    ) {
        Timber.i("Network success, saving result to database")
        saveNetworkResult(currentResult, networkResult)
        emit(Resource.success())
    }

    private suspend fun FlowCollector<Resource<Unit>>.onFetchFailed(errorMessage: String?) {
        Timber.i("Network failure $errorMessage")
        emit(Resource.error(errorMessage ?: "Unknown error"))
    }

    protected abstract fun loadFromDb(): Flow<ResultType?>

    protected abstract suspend fun getNextPage(result: ResultType?): Int?

    protected abstract suspend fun fetchFromNetwork(page: Int): Response<RequestType>

    protected abstract suspend fun saveNetworkResult(currentResult: ResultType, item: RequestType)
}
